# -*- coding: utf-8 -*-
#+STARTUP: overview indent inlineimages logdrawer
#+TITLE:       How to obtain decent screenshots of our slides
#+AUTHOR:      Arnaud Legrand

* Tools
** Deprecated approach kept for historical reasons
*** Screengrab on chrome
- The reason why I stick to screengrab is that it can use the name of
  the currently displayed page, which is quite to identify slides.
- The reason why I sticked to chrome is that on my machine, key
  shortcuts did not work for this plugin on firefox. :( Surprisingly
  it worked with chrome.
- You have to configure it so that it saves to the tmp, uses the #URL#
  for naming files, and activate fast saving so that it does not
  prompt for the file name...
- I binded the capture to Alt+Ctrl+!. It works like a charm except
  that this damned chrome puts the file in the download tab popup,
  which disables fullscreen and modifies my aspect ratio. So I had to
  set the right dimension for my chrome window so that it would not be
  an issue. To obtain a 1920x1080 screenshot, I had to set it to
  1980x1353. If figured out the resolution by trial and error and
  using the xwininfo command
  (https://ubuntuforums.org/showthread.php?t=1178429).
- Following
  https://unix.stackexchange.com/questions/5999/setting-the-window-dimensions-of-a-running-application,
  you can obtain set it to the exact dimension you want.
*** Starting the capture through X events
With the following command, I get to know what is the id of currently
active window:
#+begin_src sh :results output :exports both
xdotool getactivewindow
#+end_src
Then I can easily 
#+begin_src sh :results output :exports both
WINDOW_ID=52428801; 
while true; do 
    xdotool windowactivate --sync $WINDOW_ID key "ctrl+alt+exclam" ; 
    sleep 2 ; 
    xdotool windowactivate --sync $WINDOW_ID key Down; 
    sleep 2 ; 
done
#+end_src

Unfortunately, this damn chrome seems to miss some of the capture
events and I randomly get half of the screenshots I expect! >:(

Doing it manually, I realized I often had to wait for the image to be
captured. Damn it! This is going to be impossible to script with such
a shitty plugin. :(

Plus I realized the slide layout is not exactly the same in true
fullscreen/presentation mode than when the damned download tab is
activated. That's a pity as knowing the url when capturing would
really useful. Unfortunately, the url is not in the title.
*** Doing the job with an other capture tool.

Mmmh, I really want the url to identify my slides. So I installed
this:
https://chrome.google.com/webstore/detail/url-in-title/ignpacbgnbnkaiooknalneoeladjnfgb
and I configured it so that the {hash} appears in the title. 

Then I can simply do:
#+begin_src sh :results output :exports both
xwininfo -id 0x3400001 | grep 'Window id' | 
    sed -e 's/[^"]*"//' -e 's/ -.*//' -e 's/.*#/shot_/' -e 's/\//_/g'
#+end_src

#+RESULTS:
: shot__0_2

#+begin_src sh :results output :exports both
WINDOW_ID=0x3400001; 
sleep 2;
while true; do 
    SLIDE_ID=`xwininfo -id 0x3400001 | grep 'Window id' | 
       sed -e 's/[^"]*"//' -e 's/ -.*//' -e 's/.*#/shot_/' -e 's/\//_/g'`
    # I want to avoid overwriting previous screenshots
    name=/tmp/$SLIDE_ID  # Inspired by https://stackoverflow.com/questions/12187859/create-new-file-but-add-number-if-filename-already-exists-in-bash
    if [[ -e $name.png ]] ; then
       i=0
       while [[ -e $name-$i.png ]] ; do
          let i++
       done
       name=$name-$i
    fi
    gnome-screenshot --file=$name.png
    sleep 1 ; 
    xdotool windowactivate --sync $WINDOW_ID key Down; 
    sleep 1 ; 
done
#+end_src
** The approach that works.
Later, I realized, chrome did not handle the layout in the same way as
firefox. Damned f****ing HTML browsers! We used firefox during the
shots so let's redo this. 

Let's run the =xwininfo= command to obtain the id of the window I'm
interested in:
#+begin_src sh :results output :exports both
xwininfo | grep "Window id"
#+end_src

#+RESULTS:
: xwininfo: Window id: 0x1c00213 "[ GitPitch ] alegrand/RR_MOOC/master - http://localhost/~alegrand/PITCHME/#/ - Mozilla Firefox"

Then I can simply do:
#+begin_src sh :results output :exports both
xwininfo -id 0x1c00213  | grep 'Window id' | 
    sed -e 's/[^"]*"//' -e 's/ -[^-]*$//' -e 's/.*#/shot_/' -e 's/\//_/g'
#+end_src

#+RESULTS:
: shot__1_3

And now let's start the recording. I have to manually hit the "right
arrow" key at the end of a sequence.

#+begin_src sh :results output :exports both
WINDOW_ID=0x1800555; 
sleep 2;
while true; do 
    SLIDE_ID=`xwininfo -id $WINDOW_ID | grep 'Window id' | 
       sed -e 's/[^"]*"//' -e 's/ -[^-]*$//' -e 's/.*#/shot_/' -e 's/\//_/g'`
    # I want to avoid overwriting previous screenshots
    name=/tmp/$SLIDE_ID  # Inspired by https://stackoverflow.com/questions/12187859/create-new-file-but-add-number-if-filename-already-exists-in-bash
    # if [[ -e $name.png ]] ; then
       i=0
       while [[ -e $name-$i.png ]] ; do
          echo $name-$i.png
          let i++
       done
       name=$name-$i
    # fi
    echo "Saving to " $name-$i.png
    gnome-screenshot --file=$name.png
    sleep 1 ; 
    xdotool windowactivate --sync $WINDOW_ID key Down; 
    sleep 1 ; 
done
#+end_src

Rhaa finally. Here is what I get in the end:

#+begin_src sh :results output :exports both
ls /tmp/shot*
#+end_src

#+RESULTS:
#+begin_example
/tmp/shot__0_1-0.png
/tmp/shot__0_2-0.png
/tmp/shot__0_2-10.png
/tmp/shot__0_2-1.png
/tmp/shot__0_2-2.png
/tmp/shot__0_2-3.png
/tmp/shot__0_2-4.png
/tmp/shot__0_2-5.png
/tmp/shot__0_2-6.png
/tmp/shot__0_2-7.png
/tmp/shot__0_2-8.png
/tmp/shot__0_2-9.png
/tmp/shot__1-0.png
/tmp/shot__1_1-0.png
/tmp/shot__1_2-0.png
/tmp/shot__1_3-0.png
/tmp/shot__1_4-0.png
/tmp/shot__1_4-1.png
/tmp/shot__1_5-0.png
/tmp/shot__1_5-1.png
/tmp/shot__1_5-2.png
/tmp/shot__1_5-3.png
/tmp/shot__1_5-4.png
/tmp/shot__1_6-0.png
/tmp/shot__1_6-1.png
/tmp/shot__1_6-2.png
/tmp/shot__1_7-0.png
/tmp/shot__1_7-1.png
/tmp/shot__1_7-4.png
/tmp/shot__2_1-0.png
/tmp/shot__2_2-0.png
/tmp/shot__2_3-0.png
/tmp/shot__2_3-1.png
/tmp/shot__2_3-2.png
/tmp/shot__2_4-0.png
/tmp/shot__2_4-1.png
/tmp/shot__2_4-2.png
/tmp/shot__2_4-3.png
/tmp/shot__2_5-0.png
/tmp/shot__2_6-0.png
/tmp/shot__2_7-0.png
/tmp/shot__2_7-1.png
/tmp/shot__2_7-2.png
/tmp/shot__2_7-3.png
/tmp/shot__2_7-4.png
/tmp/shot__2_8-0.png
/tmp/shot__2_8-1.png
/tmp/shot__2_9-0.png
/tmp/shot__3-0.png
/tmp/shot__3_1-0.png
/tmp/shot__3_2-0.png
/tmp/shot__3_3-0.png
/tmp/shot__3_3-1.png
/tmp/shot__3_3-2.png
/tmp/shot__3_3-3.png
/tmp/shot__3_3-4.png
/tmp/shot__3_3-5.png
/tmp/shot__3_4-0.png
/tmp/shot__3_5-0.png
/tmp/shot__3_5-1.png
/tmp/shot__3_5-2.png
/tmp/shot__3_5-3.png
/tmp/shot__3_5-4.png
/tmp/shot__3_5-5.png
/tmp/shot__3_5-6.png
/tmp/shot__3_6-0.png
/tmp/shot__3_6-1.png
/tmp/shot__3_6-2.png
/tmp/shot__4_1-0.png
/tmp/shot__4_2-0.png
/tmp/shot__5_10-0.png
/tmp/shot__5_1-0.png
/tmp/shot__5_2-0.png
/tmp/shot__5_3-0.png
/tmp/shot__5_4-0.png
/tmp/shot__5_5-0.png
/tmp/shot__5_6-0.png
/tmp/shot__5_7-0.png
/tmp/shot__5_8-0.png
/tmp/shot__5_9-0.png
/tmp/shot__6_1-0.png
/tmp/shot__6_2-0.png
/tmp/shot__7_1-0.png
/tmp/shot__7_2-0.png
/tmp/shot__7_3-0.png
/tmp/shot__7_3-1.png
/tmp/shot__7_3-2.png
/tmp/shot__7_4-0.png
/tmp/shot__7_5-0.png
/tmp/shot__7_6-0.png
/tmp/shot__7_7-0.png
/tmp/shot__7_8-0.png
/tmp/shot__7_8-1.png
/tmp/shot__7_8-2.png
/tmp/shot__7_8-3.png
/tmp/shot__7_9-0.png
/tmp/shot__8_1-0.png
/tmp/shot__8_2-0.png
/tmp/shot__8_3-0.png
#+end_example

#+begin_src sh :results output :exports both
mv /tmp/shot* ./screenshots/module2
#+end_src

#+RESULTS:

** Using a better naming convention
#+begin_src sh :results output :exports both
for i in /tmp/shot*.png ; do
	 j=`echo $i | sed -e 's/shot__\([0-9]*\)_\([0-9]*\)-\([0-9]*\).png/C028AL_W1_S\1-\2-\3.png/'`
	 mv $i $j;
done
#+end_src

#+RESULTS:

