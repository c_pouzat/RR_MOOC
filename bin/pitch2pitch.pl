#!/usr/bin/perl

$first_slide=1;

while(defined($line=<>)) {
    chomp($line);
    if($first_slide && (($line =~ /^---/) || ($line =~ /^\+\+\+/))) {
	$first_slide = 0;
	next;
    }
    # +++?image=./assets/md/assets/les_decodeurs.png&size=contain
    if($line=~/^(.*)\?image=(.*)&size=(.*)$/) {
	print $1."\n".'<!-- .slide: data-background-image="'.$2.'" data-background-size="'.$3.'" -->'."\n";
    } else {
	print $line."\n";
    }
}

