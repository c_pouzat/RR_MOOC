#!/usr/bin/perl

$prevline = <>;
chomp($prevline);
while(defined($line=<>)) {
    chomp($line);
#    print "\t '$prevline' | '$line' \n";
    if($prevline =~ /^C028AL-([^;]*); (.*)$/) {
    	$sequence_id = "C028AL-".$1;
    	$sequence_title = $2;
    	$prevline = $sequence_id."<br><br>".$sequence_title;
    }
    $line =~ s/&lt;/</g;
    $line =~ s/&gt;/>/g;
    if($line=~/^---$/) { # This looks like a real slide. Let's leave it as such
	print $prevline."\n";
    } elsif(($line=~/^==*$/) or ($line=~/^\-\-*$/)) { # This is a section or subsection head
	if($line =~/^==*$/) {
	    $head = '---';
	} else {
	    $head = '+++';
	}
	$head_append = "";
	if($prevline =~ /^(.*)<(.*)>$/) {
	    $prevline = $1;
	    $head_append = $2;
	    $head_append =~ s/^file://;
	    $head_append = "?image=".$head_append;
	    if($prevline =~ /^\s*$/) { # empty title, do not underline...
		$line = "";
	    }
	}
	print $head.$head_append."\n";
	print $prevline."\n";
    } else {
	# if(!($prevline =~/^$/)) { # pandoc introduces line feed where it should not
	print $prevline."\n";
	# }
    }
    $prevline = $line;
}
print $prevline."\n";
