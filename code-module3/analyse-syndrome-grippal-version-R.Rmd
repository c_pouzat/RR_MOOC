---
title: "Incidence du syndrome grippal"
output:
       pdf_document:
         toc: true
       html_document:
         toc: true
         theme: journal
header-includes:
- \usepackage[french]{babel}
- \usepackage[upright]{fourier}
- \hypersetup{colorlinks=true,pagebackref=true}
---

# Les paquets additionnels

Nous allons devoir manipuler des dates au format `ISO 8601`. Il y quelques manières de le faire avec `R` et nous allons utiliser le paquet [parsedate](https://cran.r-project.org/package=parsedate) qu'il faudra installer si ce n'est pas déjà fait (`install.packages("parsedate")`) puis charger dans `R` :

```{r charge-parsedate}
library(parsedate)
```

Nous allons également devoir manipuler des suites chronologiques (_time series_) irrégulières, ce que nous allons faire avec le paquet `zoo` qu'il faudra installer si ce n'est pas déjà fait (`install.packages("zoo")`) puis charger dans `R` :

```{r charge-zoo}
library(zoo)
```

Nous allons de plus « demander » à `R` d'écrire les nombres décimaux en français — c'est-à-dire avec une virgule entre parties entière et décimale — et d'utiliser un nombre de colonnes plus large que celui utiliser par défaut — ce choix est sensé pour produire le document au format `html`, pas pour une document au format `pdf` — :

```{r fixe-options}
options(OutDec=",")
options(width=150)
```

# Les données

Les données de l'incidence du syndrome grippal sont disponibles du site Web du [Réseau Sentinelles](http://www.sentiweb.fr/). Nous les récupérons sous forme d'un fichier en format CSV dont chaque ligne correspond à une semaine de la période demandée. Les dates de départ et de fin sont codées dans l'URL: "wstart=198501" pour semaine 1 de l'année 1985 et "wend=201730" pour semaine 30 de l'année 2017. La première ligne du fichier CSV est un commentaire, que nous ignorons en précisant `skip=1`.

```{r lis-les-donnees}
raw_data = read.csv("http://websenti.u707.jussieu.fr/sentiweb/api/data/rest/getIncidenceFlat?indicator=3&wstart=198501&wend=201730&geo=PAY1&$format=csv",
                    skip=1)
head(raw_data)						
```

Y a-t-il des points manquants dans ce jeux de données ? Oui, la semaine 19 de l'année 1989 n'a pas de valeurs associées.

```{r donnees-manquantes}
raw_data[apply(raw_data,1,function(x) any(is.na(x))),]
```

`R` est un langage avec lequel on peut faire ce que l'on veut, mais il a été conçu par des statisticiens et permet de ce fait de gérer les données manquantes — _not available_ ou `NA` dans le jargon `R` ; elles sont fréquentes en statistique avec par exemple le cas de personnes refusant de répondre à une ou plusieurs questions lors d'un sondage ; de plus en plus fréquentes aujourd'hui avec les données collectées par les « objets connectés ». Nous pouvons ainsi garder cette ligne est utiliser les arguments adéquats des fonctions dont nous avons besoin. Nous pourrions également éliminer ce point avec :

```{r data-tout-est-la, eval=FALSE}
data = raw_data[!apply(raw_data,1,function(x) any(is.na(x))),]
```

mais nous n'allons pas le faire dans ce qui suit.

Nos données utilisent une convention inhabituelle: le numéro de semaine est collé à l'année, donnant l'impression qu'il s'agit de nombre entier. C'est comme ça que `R` les interprète dans le `data.frame` que nous avons implicitement construit lors de l'importation des données.

Un deuxième problème est que `zoo` que nous allons utiliser pour manipuler les données comme des suites chronologiques (_time series_) ne comprend pas les numéros de semaine. Il faut lui fournir les dates de début de semaine. Nous utilisons pour cela la bibliothèque `parsedate`.

Comme la conversion des semaines est devenu assez complexe, nous écrivons une petite fonction `R` pour cela. Cette fonction commence par réécrire une date comme `198907` (la 7e semaine de l'année 1989) de la façon suivante : `1989-W07` qui est comprise par `parsedate`.

```{r def-converti_week}
converti_week = function(w) {
	ws = paste(w)
	iso = paste0(substring(ws,1,4),"-W",substring(ws,5,6))
	as.Date(parse_iso_8601(iso))
	}
```

Nous créons ensuite un objet de classe `zoo`, c'est-à-dire un objet dont la classe est adaptée aux suites chronologiques non uniformément échantillonnées. Il y aussi une « bizarrerie » propre à `R` dont il faut tenir compte : le fait que le contenu de la colonne `inc` (incidence) a été converti en facteur (`factor`) lors de l'importation ; cela explique le caractère quelque peu « tordu » du premier argument de la fonction `zoo` ci-dessous :

```{r inc}
inc = zoo(as.integer(levels(raw_data$inc)[as.integer(raw_data$inc)]),as.Date(sapply(raw_data$week,converti_week)))
```

Ici `zoo` nous avertit que nos données contiennent des `NA` ce que nous savions déjà. Maintenant que les données sont disponibles sous forme d'une suite chronologique de classe `zoo`, nous pouvons appeler la _fonction générique_ `plot` qui se chargera d'appeler la méthode adaptée à la construction d'un graphe^[`R` a été développé _consciemment_ avec l'idée de fonctions génériques : « One important tool is a general way to define methods. Methods organize software in a conceptual table of generic functions by signatures for classes of the arguments to the functions. » John Chambers (1999) [Computing with Data: Concepts and Challenges](http://statweb.stanford.edu/~jmc4/pub.html) _The American Statistician_, 53:1, pp. 73-84. Les approches « modernes » des « plus jeunes » qui pensent que la méthode n'est bonne que si elle frappe l'utilisateur comme « le coup de poing de Mohamed Ali sur le visage de George Forman » — je pense ici à `ggplot2` — montrent juste qu'ils n'ont pas compris les idées de J. Chambers (qu'ils n'ont de toute façon pas lu). Avec la _fonction générique_, l'utilisateur manipule des données dont la structure peut être compliquée, il sait que le langage dispose d'une fonction nommée `plot` pour générer un graphe _adapté à la structure des données_ et _c'est tout ce qu'il a besoin de savoir_.] :

```{r plot-inc}
plot(inc,xlab="Année",ylab="Incidence")
```

Pour « zoomer » sur la partie des données postérieure au 1er juillet 2013, il suffit de taper :

```{r plot-inc-zoom}
plot(inc[time(inc) > "2013-07-01"],xlab="Année",ylab="Incidence")
```

# Étude de l'incidence annuelle

Étant donné que le pic de l'épidémie se situe en hiver, à cheval entre deux années civile, nous définissons la période de référence entre deux minima de l'incidence, du 1er août de l'année NN au 1er août de l'année NN+1.

Nous pouvons faire cela de façon relativement simple avec `R` en extrayant la séquence des temps de notre `inc` (c'est ce que fait la fonction `time` ci-dessous) puis en sélectionnant les dates situées entre les deux limites spécifiées ci-dessus. Nous effectuons la somme des cas sur l'année avec la fonction `sum` et nous utilisons l'argument optionel `na.rm=TRUE`, puisqu'une de nos entrée est `NA`. Nous créons ensuite un nouvel objet de classe `zoo` contenant les incidences annuelles et en attribuant l'année de la fin de la période considérée à chaque incidence annuelle :

```{r incidence_annuelle}
incidence_annuelle = sapply(1985:2016, function(y) sum(inc[time(inc) >= paste0(y,"-08-01") & time(inc) < paste0(y+1,"-08-01")],na.rm=TRUE))
incidence_annuelle = zoo(incidence_annuelle,1986:2017)
```

Le graphe des données est obtenu comme précédemment :

```{r plot-incidence_annuelle}
plot(incidence_annuelle,xlab="Année",ylab="Incidence annuelle",pch="*",type="p")
```

On remarque que la donnée manquante (7e semaine de l'année 1989) est précisément dans l'année ou la plus forte incidence est constatée (cette plus forte incidence est donc en fait sous estimée). Une liste triée permet de plus facilement répérer les valeurs les plus élevées (à la fin).

```{r ordonne-incidence_annuelle}
idx = sort.int(unclass(incidence_annuelle),index.return = TRUE)$ix
cbind(time(incidence_annuelle)[idx],unclass(incidence_annuelle)[idx])
```

Enfin, un histogramme montre bien que les épidémies fortes, qui touchent environ 10% de la population française, sont assez rares: il y en eu trois au cours des 35 dernières années.


```{r hist-incidence_annuelle}
hist(incidence_annuelle,breaks=10,xlab="Incidence annuelle",ylab="Nb d'observations",main="")
```


