# C028AL-M4-S4  (≅ 6:30 minutes)

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/clap.png" style="width:50%; margin-top: none; border: none; background: none; box-shadow: none;" />

+++
### 4. Vers une étude reproductible&emsp14;: la réalité du terrain

 1. Introduction
 2. L'enfer des données
 3. L'enfer du logiciel
 4. **L'enfer du calcul**
     - L'arithmétique à virgule flottante
     - Les plateformes de calcul
     - Les nombres aléatoires

Note:
  Dans cette séquence, nous montrons les difficultés spécifiques qu'on
  rencontre dans le calcul numérique. La plus fréquente est due à
  l'arithmétique à virgule flottante, ce qui est un cas spécial de la
  variabilité entre les plateformes de calcul. Une autre difficulté
  est due aux générateurs de nombres aléatoires.

+++
## L'arithmétique à virgule flottante

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/polynome1.svg" style="width:70%; margin-top: none; border: none; background: none; box-shadow: none;" />
```
def polynome(x):
    return x**9 - 9.*x**8 + 36.*x**7 - 84.*x**6 + 126.*x**5 \
           - 126.*x**4 + 84.*x**3 - 36.*x**2 + 9.*x - 1.
```

Note:
  Commençons avec un exemple. Mon collègue Arnaud a calculé les valeurs d'un
  polynôme d'ordre 9. Le voici.

+++
## L'arithmétique à virgule flottante

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/polynome2.svg" style="width:70%; margin-top: none; border: none; background: none; box-shadow: none;" />
```
def horner(x):
    return x*(x*(x*(x*(x*(x*(x*(x*(x - 9.) + 36.) - 84.) + 126.) \
           - 126.) + 84.) - 36.) + 9.) - 1.
```

Note:
  Mon collègue Christophe lui fait remarquer que la méthode de Horner résulte dans
  un calcul plus efficace. Il s'agit d'un réarrangement des termes qui reduit notamment
  le nombre de multiplications. Le voici. Les deux courbes se recouvrent, donc les
  résultats sont identiques.

+++
## L'arithmétique à virgule flottante

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/polynome3.svg" style="width:70%; margin-top: none; border: none; background: none; box-shadow: none;" />
```
def simple(x):
    return (x-1.)**9
```

Note:
  Une analyse un peu plus approfondie montre que notre polynôme a une seule racine
  à x=1, ce qui nous permet d'en simplifier le calcul. Nous avons maintenant
  trois courbes qui se recouvrent. Tout va bien !

+++
## L'arithmétique à virgule flottante

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/polynome3-4.svg" style="width:70%; margin-top: none; border: none; background: none; box-shadow: none;" />

Note:
  Mais regardons la région autour de x=1 de plus près...

+++
## L'arithmétique à virgule flottante

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/polynome4.svg" style="width:70%; margin-top: none; border: none; background: none; box-shadow: none;" />

Note:
  Ça a l'air un peu bizarre. Et les trois courbes ne sont enfin pas identiques,
  juste proches. Qu'est-ce qui se passe ?

+++
## L'arrondi

<ul>
 <li>Il y a un arrondi implicite dans chaque opération.</li>
 <li class="fragment">a+b est en réalité *arrondi*(a+b).</li>
 <li class="fragment">Mais *arrondi*(*arrondi*(a+b)+c) ≠ *arrondi*(a+*arrondi*(b+c)).</li>
 <li class="fragment">L'ordre des opérations est donc important.</li>
</ul>
 
<p class="fragment"> **Pour un calcul reproductible,<br /> il faut préserver l'ordre des opérations&emsp14;!!**</p>

Note:
  Le résultat d'une opération arithmétique sur des flottant n'est en général pas
  représentable par un autre flottant. On applique une opération d'arrondi qui
  remplace le résultat exact avec une valeur proche qui est représentable.
  
  A cause de l'arrondi, les règles habituelles de l'arithmétique ne sont plus
  applicables à l'identique. On perd notamment l'associativité de l'addition et
  de la multiplication.
  
  En changeant l'ordre des opérations, on peut donc changer le résultat !

+++
## Comment l'expliquer à mon compilateur&emsp14;?

Pour accélérer le calcul, les compilateurs peuvent changer l'ordre des opérations,
et donc le résultat.
 
<p class="fragment">
Pour un calcul reproductible, il y a deux approches&emsp14;:</p>

<ul>
  <li class="fragment">
  Insister sur le respect de l'ordre des opérations, <br >
  &emsp14;si le langage le permet.<br />
  &emsp14;Exemple&emsp14;: Le module `ieee_arithmetic` en Fortran 2003 <br />
  <br /></li>
  <li class="fragment">
  Rendre la compilation reproductible&emsp14;:<br />
- Noter la version précise du compilateur<br />
- Noter toutes les options de compilation</li>
</ul>

Note:

  Ceci devient un obstacle à la reproductibilité à cause des
  optimisations appliquées par les compilateurs pour accélérer le
  calcul. Celle-ci changent souvent l'ordre des opérations et donc le
  résultat.

  Pour rendre un calcul reproductible, il y a deux
  approches. Premièrement, on peut dire à son compilateur de ne pas
  toucher à l'ordre des opérations, idéalement dans le code source du
  logiciel. Ceci est possible par exemple dans le langage Fortran
  2003.
  
  L'autre approche est de rendre la compilation elle-même
  reproductible en notant la version précise du compilateur utilisé,
  ainsi que toutes les options de compilation. Idéalement, il faut
  alors archiver le compilateur avec le logiciel de calcul.

+++
## Calcul parallèle

<ul>
 <li> Objectif&emsp14;: une exécution plus rapide <br />
    ⟶ Minimiser la communication entre processeurs <br />
    ⟶ Adapter la répartition des données... <br />
    ⟶ et donc l'ordre des opérations <br />
    <br /> </li>
 
 <li class="fragment"> Conséquence&emsp14;: le résultat dépend du nombre de processeurs&emsp14;! </li>

</ul>

<p class="fragment"> **Minimiser cet impact du parallélisme<br /> reste un sujet de recherche.**</p>

Note:

  Un autre obstacle à la reproductibilité résulte du calcul parallèle.
  Celui-ci a comme seul objectif une exécution plus rapide. Pour y arriver,
  il faut minimiser la communication entre les processeurs en adaptant
  la répartition des données. Ceci implique d'adapter l'ordre des opérations,
  car chaque processeur traite d'abord ses données locales avant d'entrer en
  communication avec les autres processeurs.
  
  La conséquence est que le résultat du calcul change avec le nombre
  de processeurs utilisés.
  
  On peut tenter d'écrire le logiciel tel que l'impact de cette variation
  soit minimisé. Mais il n'y a pour l'instant aucune technique éprouvée
  pour y arriver. Ceci reste un sujet de recherche.

+++
## Calcul parallèle&emsp14;: exemple

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/gouttedo1.png" style="width:100%; margin-top: none; border: none; background: none; box-shadow: none;" />

<font size="5">(Source&emsp14;: Thèse de Rafife Nheili, Université de Perpignan, 2016)</font>

Note:

  Regardons un simple exemple: la simulation des vagues causées dans
  un bassin carré d'eau par une goutte qui tombe au milieu. A gauche,
  la simulation exécutée sur un seul processeur. A droite, le résultat
  d'un calcul sur quatre proecesseurs, ou les points qui diffèrent
  sont colorés en gris.
  
+++
## Calcul parallèle&emsp14;: exemple

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/gouttedo2.png" style="width:100%; margin-top: none; border: none; background: none; box-shadow: none;" />

<font size="5">(Source&emsp14;: Thèse de Rafife Nheili, Université de Perpignan, 2016)</font>

Note:

  Un pas de temps plus loin, le nombre de points à résultat différent
  a nettement augmenté. Il continuera a augmenter au cours de la
  simulation, jusqu'à ce qu'il n'y a plus aucun point pour lequel le
  résultat restera identique.
  
+++
## Les plateformes de calcul

<ul>
 <li> Plateforme de calcul&emsp14;: matériel + infrastructure logicielle</li>
 <li class="fragment">Calcul = plateforme + logiciel + données </li>
 <li class="fragment">La plateforme définit l'interprétation du logiciel.</li>
 <li class="fragment">Plateforme et logiciel définissent l'interprétation des données.</li>
 <li class="fragment">Autres aspects définis par la plateforme&emsp14;:<br />
- la représentation des entiers (16/32/64 bits)<br />
- la gestion des erreurs</li>
</ul>

Note:
  Les obstacles à la reproductibilité dûs à l'arithmétique à virgule flottante
  sont un cas spécial important de la dépendance des plateformes. Un logiciel
  est écrit dans un langage de programmation, mais l'interprétation précise de
  ce langage est définie par la plateforme de calcul utilisé. Celle-ci consiste
  du matériel, notamment du processeur, et de l'infrastructure logiciel:
  système d'exploitation, compilateurs, etc.

  Un calcul est défini par trois ingrédients: la plateforme, le logiciel, et les
  données traitées. La plateforme définit comment est interprété le logiciel,
  et les deux ensembles définissent comment sont interprétées les donées.

  L'arithmétique à virgule flottante est interprétée différemment par les différentes
  plateformes de calcul populaires aujourd'hui, mais ce n'est pas le seul point
  de divergence. Dans certains langage comme le C ou le Fortran, les entiers sont
  aussi concernés car les plateformes leur attribuent des tailles et donc des
  intervalles de valeurs différentes. Enfin, les erreurs de calcul, comme par
  exemple la division par zéro, ne sont pas gérées de la ême façon par toutes les
  plateformes.
+++
## Les nombres aléatoires

<ul>
 <li> Utilisés pour simuler les processus stochastiques.<br />
 <br /></li>
 <li class="fragment"> En pratique&emsp14;: nombres **pseudo-**aléatoires.<br />
 <br /></li>
 <li class="fragment"> Suite de nombres **en apparence** aléatoires...<br />
 <br /></li>
 <li class="fragment">... mais générés par un algorithme déterministe.<br />
 <br /></li>
</ul>

+++
## Générateur de nombres pseudo-aléatoires

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/nombres-aleatoires-1.svg" style="width:70%; margin-top: none; border: none; background: none; box-shadow: none;" class="fragment current-visible" />

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/nombres-aleatoires-2.svg" style="width:70%; margin-top: none; border: none; background: none; box-shadow: none;" class="fragment current-visible" />

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/nombres-aleatoires-3.svg" style="width:70%; margin-top: none; border: none; background: none; box-shadow: none;" class="fragment current-visible" />

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/nombres-aleatoires-4.svg" style="width:70%; margin-top: none; border: none; background: none; box-shadow: none;" class="fragment current-visible" />

Note:
  On part d'un état initial appelé "graine" (typiquement un entier).

  Pour générer un nombre, on applique une transformation à cet état,
  
  puis on calcule le nombre comme fonction de cet état.

  On repète autant que nécessaire.

+++
## La reproductibilité en théorie

<ul>
 <li> Principe&emsp14;: même graine + même algorithme → même suite<br />
      <br /></li>
 <li class="fragment"> La graine est souvent choisie comme fonction de l'heure<br />
      <br /></li>
 <li class="fragment"> Il faut la définir dans le code d'application<br />
      <br /></li>
<ul>

Note:

  En principe, il suffit d'utiliser la même graine et le même algorithme pour
  toujours avoir la même suite de nombres.

  L'algorithme fait partie d'une librairie, dont il faut noter la version
  utilisée. Les librairie de nombres aléatoires choisissent souvent la graine
  comme fonction de l'heure, pour donner une apparence "plus aléatoire".
  
  Mais pour la reproductibilité, il faut définir la graine dans le code
  d'application.

+++
## La reproductibilité en pratique

<ul>
 <li> Même graine + même algorithme → même suite&emsp14;: <br />
   &emsp14;pas évident en arithmétique à virgule flottante&emsp14;! <br />
   <br /></li>
 <li class="fragment">
   Un astuce simple pour permettre la vérification&emsp14;: <br />
   &emsp14;tester quelques valeurs du début de la suite.</li>
</ul>

Note:

  En pratique, il n'est pas si facile d'obtenir une suite identique, même avec
  la même version de la même librairie, à cause de la variation dans l'arithmétique
  à virgule flottante.
  
  Un astuce simple est de tester quelques valeurs du début de la suite. Si elle ne
  sont pas identiques, on sait que la suite a changé.

+++
## Exemple&emsp14;: le langage Python

```
import random

random.seed(123)
for i in range(5):
    print(random.random())
```

```
0.052363598850944326
0.08718667752263232
0.4072417636703983
0.10770023493843905
0.9011988779516946
```

Note:

  Voici l'application de cet astuce en langage Python. On commence par
  définir la graine et d'afficher les premières valeurs de la suit de
  nombres pseudo-aléatoires.
  
+++
## Exemple&emsp14;: le langage Python

```
import random

random.seed(123)
assert random.random() == 0.052363598850944326
assert random.random() == 0.08718667752263232
assert random.random() == 0.4072417636703983
```

Note:

  Puis on modifie le code en remplaçant l'affichage des valeurs par des
  comparaisons aux valeurs précédemment affichées. En cas de divergence,
  le programme s'arrête avec un message d'erreur.

+++
## Les points clés à retenir

<ul>
 <li>Les résultats d'un calcul dépendent <br />
- du logiciel<br />
- des données d'entrée<br />
- de la plateforme de calcul&emsp14;: matériel, compilateurs, ... </li>
 <li class="fragment">L'influence de la plateforme est importante pour l'arithmétique à virgule flottante.</li>
 <li class="fragment"> Notez les paramètres dont peuvent dépendre vos résultats&emsp14;: <br />
- version du compilateur, options de compilation <br />
- matériel (type de processeur, GPU, ...) <br />
- nombre de processeurs </li>
 <li class="fragment">En utilisant un générateur de nombres aléatoires, définissez la graine et vérifiez les premiers éléments de la suite.</li>
</ul>
