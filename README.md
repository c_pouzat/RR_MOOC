# MOOC on Reproducible Research

This website gathers various [thoughts](./brainstorm.org) (in French) and documents
produced when discussing about organizing a a MOOC on Reproducible
Research on the [FUN platform](https://www.fun-mooc.fr/about) in the
context of the [Inria Learning Lab](https://learninglab.inria.fr/)

Authors:
- Aurélie Bayle
- Marie-Hélène Comte
- Konrad Hinsen
- Arnaud Legrand
- Christophe Pouzat

## Links for slide visualization via [GitPitch](https://gitpitch.com/)

  - [Module 1](https://gitpitch.com/alegrand/RR_MOOC/master?p=slides-module1)
  - [Module 2](https://gitpitch.com/alegrand/RR_MOOC/master?p=slides-module2)
  - Module 3:  [français](https://gitpitch.com/alegrand/RR_MOOC/master?p=slides-module3)/[anglais](https://gitpitch.com/alegrand/RR_MOOC/master?p=slides-module3-en)
  - [Module 4](https://gitpitch.com/alegrand/RR_MOOC/master?p=slides-module4):
    - [Séquence 1](https://gitpitch.com/alegrand/RR_MOOC/master?p=slides-module4-sequence1)
    - Séquence 4: [français](https://gitpitch.com/alegrand/RR_MOOC/master?p=slides-module4-sequence4)/[anglais](https://gitpitch.com/alegrand/RR_MOOC/master?p=slides-module4-sequence4-en)
