#+TITLE: Incidence de la varicelle
#+LANGUAGE: fr
#+OPTIONS: *:nil num:1 toc:t

# #+HTML_HEAD: <link rel="stylesheet" title="Standard" href="http://orgmode.org/worg/style/worg.css" type="text/css" />
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="http://www.pirilampo.org/styles/readtheorg/css/htmlize.css"/>
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="http://www.pirilampo.org/styles/readtheorg/css/readtheorg.css"/>
#+HTML_HEAD: <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
#+HTML_HEAD: <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="http://www.pirilampo.org/styles/lib/js/jquery.stickytableheaders.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="http://www.pirilampo.org/styles/readtheorg/js/readtheorg.js"></script>

#+PROPERTY: header-args  :session  :exports both

* Préface

Pour exécuter le code de cette analyse, il faut disposer des logiciels suivants:

** Emacs 25 ou 26
Une version plus ancienne d'Emacs devrait suffire, mais en ce cas il est prudent d'installer une version récente (9.x) d'org-mode.
** Python 3.6
Nous utilisons le traitement de dates en format ISO 8601, qui a été implémenté en Python seulement avec la version 3.6.

#+BEGIN_SRC python :results output
import sys
if sys.version_info.major < 3 or sys.version_info.minor < 6:
    print("Veuillez utiliser Python 3.6 (ou plus) !")
#+END_SRC

#+BEGIN_SRC emacs-lisp :results output
(unless (featurep 'ob-python)
  (print "Veuillez activer python dans org-babel (org-babel-do-languages) !"))
#+END_SRC

** R 3.4
Nous n'utilisons que des fonctionnalités de base du langage R, une version antérieure devrait suffire.

#+BEGIN_SRC emacs-lisp :results output
(unless (featurep 'ob-R)
  (print "Veuillez activer R dans org-babel (org-babel-do-languages) !"))
#+END_SRC

* Préparation des données

Les données de l'incidence de la varicelle sont disponibles du site Web du [[http://www.sentiweb.fr/][Réseau Sentinelles]]. Nous les récupérons en format CSV dont chaque ligne correspond à une semaine de la période demandée. Les dates de départ et de fin sont codées dans l'URL: "wstart=199101" pour semaine 1 de l'année 1991 et "wend=201820" pour semaine 20 de l'année 2018. L'URL complet est:
#+NAME: data-url
https://websenti.u707.jussieu.fr/sentiweb/api/data/rest/getIncidenceFlat?indicator=7&wstart=199101&wend=201820&geo=PAY1&$format=csv

Voici l'explication des colonnes donnée sur le site d'origine:

| Nom de colonne | Libellé de colonne                                                                                                                |
|----------------+-----------------------------------------------------------------------------------------------------------------------------------|
| ~week~       | Semaine calendaire (ISO 8601)                                                                                                     |
| ~indicator~  | Code de l'indicateur de surveillance                                                                                              |
| ~inc~        | Estimation de l'incidence de consultations en nombre de cas                                                                       |
| ~inc_low~    | Estimation de la borne inférieure de l'IC95% du nombre de cas de consultation                                                     |
| ~inc_up~     | Estimation de la borne supérieure de l'IC95% du nombre de cas de consultation                                                     |
| ~inc100~     | Estimation du taux d'incidence du nombre de cas de consultation (en cas pour 100,000 habitants)                                   |
| ~inc100_low~ | Estimation de la borne inférieure de l'IC95% du taux d'incidence du nombre de cas de consultation (en cas pour 100,000 habitants) |
| ~inc100_up~  | Estimation de la borne supérieure de l'IC95% du taux d'incidence du nombre de cas de consultation (en cas pour 100,000 habitants) |
| ~geo_insee~  | Code de la zone géographique concernée (Code INSEE) http://www.insee.fr/fr/methodes/nomenclatures/cog/                            |
| ~geo_name~   | Libellé de la zone géographique (ce libellé peut être modifié sans préavis)                                                       |

L'indication d'une semaine calendaire en format [[https://en.wikipedia.org/wiki/ISO_8601][ISO-8601]] est populaire en Europe, mais peu utilisée aux Etats-Unis. Ceci explique peut-être que peu de logiciels savent gérer ce format. Le langage Python le fait depuis la version 3.6. Nous utilisons donc ce langage pour la préparation de nos données.

** Téléchargement
Pour nous protéger contre une éventuelle disparition ou modification du serveur du Réseau Sentinelles, nous faisons une copie locale de ce jeux de données que nous préservons avec notre analyse. Il est inutile et même risquée de télécharger les données à chaque exécution, car dans le cas d'une panne nous pourrions remplacer nos données par un fichier défectueux. Pour cette raison, nous téléchargeons les données seulement si la copie locale n'existe pas.

#+BEGIN_SRC python :results output :var data_url=data-url
data_file = "varicelle.csv"

import os
import urllib.request
if not os.path.exists(data_file):
    urllib.request.urlretrieve(data_url, data_file)
#+END_SRC

Nous commençons le traitement par l'extraction des données qui nous intéressent. D'abord nous découpons le contenu du fichier en lignes, dont nous jetons la première qui ne contient qu'un commentaire. Les autres lignes sont découpées en colonnes.

#+BEGIN_SRC python :results output
data = open(data_file).read()
lines = data.strip().split('\n')
data_lines = lines[1:]
table = [line.split(',') for line in data_lines]
#+END_SRC

Regardons ce que nous avons obtenu:
#+BEGIN_SRC python :results value
table[:5]
#+END_SRC

** Extraction des colonnes utilisées
Il y a deux colonnes qui nous intéressent: la première (~"week"~) et la troisième (~"inc"~). Nous vérifions leurs noms dans l'en-tête, que nous effaçons par la suite. Enfin, nous créons un tableau avec les deux colonnes pour le traitement suivant.
#+BEGIN_SRC python :results silent
week = [row[0] for row in table]
assert week[0] == 'week'
del week[0]
inc = [row[2] for row in table]
assert inc[0] == 'inc
del inc[0]
raw_data = list(zip(week, inc))
#+END_SRC

Regardons les premières et les dernières lignes. Nous insérons ~None~ pour indiquer à org-mode la séparation entre les trois sections du tableau: en-tête, début des données, fin des données.
#+BEGIN_SRC python :results value
[('week', 'inc'), None] + raw_data[:5] + [None] + raw_data[-5:]
#+END_SRC

** Vérification
Il est toujours prudent de vérifier si les données semblent crédibles. Nous savons que les semaines sont données par six chiffres (quatre pour l'année et deux pour la semaine), et que les incidences sont des nombres entiers positifs.
#+BEGIN_SRC python :results output
for week, inc in raw_data:
    if len(week) != 6 or not week.isdigit():
        print("Valeur suspecte dans la colonne 'week': ", (week, inc))
    if not inc.isdigit():
        print("Valeur suspecte dans la colonne 'inc': ", (week, inc))
#+END_SRC

La vérification n'a pas montré de point manquant dans le jeux de données.
#+BEGIN_SRC python :results silent
valid_data = raw_data
#+END_SRC

** Conversions
Pour faciliter les traitements suivants, nous remplaçons les numéros de semaine ISO par les dates qui correspondent aux lundis. A cette occasion, nous trions aussi les données par la date, et nous transformons les incidences en nombres entiers.

#+BEGIN_SRC python :results silent
import datetime
data = [(datetime.datetime.strptime(year_and_week + ":1" , '%G%V:%u').date(),
         int(inc))
        for year_and_week, inc in valid_data]
data.sort(key = lambda record: record[0])
#+END_SRC

Regardons de nouveau les premières et les dernières lignes:
#+BEGIN_SRC python :results value
str_data = [(str(date), str(inc)) for date, inc in data]
[('date', 'inc'), None] + str_data[:5] + [None] + str_data[-5:]
#+END_SRC

** Vérification des dates
Nous faisons encore une vérification: nos dates doivent être séparées d'exactement une semaine, sauf autour du point manquant.
#+BEGIN_SRC python :results output
dates = [date for date, _ in data]
for date1, date2 in zip(dates[:-1], dates[1:]):
    if date2-date1 != datetime.timedelta(weeks=1):
        print(f"Il y a {date2-date1} entre {date1} et {date2}")
#+END_SRC

** Passage Python -> R
Nous passons au langage R pour inspecter nos données. Nous utilisons le mécanisme d'échange de données proposé par org-mode, ce qui nécessite un peu de code Python pour transformer les données dans le bon format.

#+NAME: data-for-R
#+BEGIN_SRC python :results silent
[('date', 'inc'), None] + [(str(date), inc) for date, inc in data]
#+END_SRC

En R, les données arrivent sous forme d'un data frame, mais il faut encore convertir les dates, qui arrivent comme chaînes de caractères.
#+BEGIN_SRC R :results output :var data=data-for-R
data$date <- as.Date(data$date)
summary(data)
#+END_SRC

** Inspection
Regardons enfin à quoi ressemblent nos données !
#+BEGIN_SRC R :results output graphics :file inc-plot.png
plot(data, type="l", xlab="Date", ylab="Incidence hebdomadaire")
#+END_SRC

Un zoom sur les dernières années montre mieux la situation des pics en hiver. Le creux des incidences se trouve début septembre.
#+BEGIN_SRC R :results output graphics :file inc-plot-zoom.png
plot(tail(data, 200), type="l", xlab="Date", ylab="Incidence hebdomadaire")
#+END_SRC

Pour mieux localiser les creux, regardons les incidences les plus faibles des dix dernières années.
#+BEGIN_SRC R :results value
last = tail(data, 520)
low = last[last$inc < 3000, c("date", "inc")]
low
#+END_SRC

* Étude de l'incidence annuelle

** Calcul de l'incidence annuelle
Étant donné que chaque pic de l'épidémie se situe sur deux années civiles, nous définissons la période de référence entre deux minima de l'incidence, du 1er septembre de l'année /N/ au 1er septembre de l'année /N+1/. Nous mettons l'année /N+1/ comme étiquette sur cette année décalée, car le maximum de l'épidémie se trouve toujours dans dans l'année /N+1/. Comme l'incidence de la varicelle est faible en été, cette modification ne risque pas de fausser nos conclusions.

Voici une fonction qui calcule l'incidence annuelle en appliquant ces conventions.
#+BEGIN_SRC R :results silent
pic_annuel = function(annee) {
      debut = paste0(annee-1,"-09-01")
      fin = paste0(annee,"-09-01")
      semaines = data$date > debut & data$date <= fin
      sum(data$inc[semaines])
      }
#+END_SRC

Nous devons aussi faire attention aux premières et dernières années de notre jeux de données. Les données commencent en janvier 1991, ce qui ne permet pas de quantifier complètement le pic attribué à cette année. Nous l'enlevons donc de notre analyse. Nous terminons en 2017 parce que pour 2018 nous n'avons pas encore les données de l'été.
#+BEGIN_SRC R :results silent
annees <- 1992:2017
#+END_SRC

#+BEGIN_SRC R :results value
inc_annuelle = data.frame(annee = annees,
                          incidence = sapply(annees, pic_annuel))
head(inc_annuelle)
#+END_SRC

** Inspection
Voici les incidences annuelles en graphique.
#+BEGIN_SRC R :results output graphics :file annual-inc-plot.png
plot(inc_annuelle, type="p", xlab="Année", ylab="Incidence annuelle")
#+END_SRC

** Identification des épidémies les plus fortes
Une liste triée par ordre décroissant d'incidence annuelle permet de plus facilement repérer les valeurs les plus élevées:
#+BEGIN_SRC R :results output
inc_annuelle[order(-inc_annuelle$incidence),]
#+END_SRC

Enfin, un histogramme montre que les épidémies les plus fréquentes touchent entre 600.000 et 700.000 personnes, donc un peu plus d'1% de la population française. Les variations d'une année à l'autre ne sont pas très importantes, car une épidémie ne fait jamais moins que 500.000 ou plus de 850.000 victimes.
#+BEGIN_SRC R :results output graphics :file annual-inc-hist.png
hist(inc_annuelle$incidence, breaks=10, xlab="Incidence annuelle", ylab="Nb d'observations", main="")
#+END_SRC
