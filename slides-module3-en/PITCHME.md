# C028AL-M3-S1

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/clap.png" style="width:50%; margin-top: none; border: none; background: none; box-shadow: none;" />

+++
### 3. Diving in: a replicable analysis

 1. **What is a replicable analysis?**
 2. Case study: incidence of influenza-like illness
 3. Data import <br />
    (A: Jupyter/Python, B: RStudio, C: Org mode)
 4. Verification and inspection <br />
    (A: Jupyter/Python, B: RStudio, C: Org mode)
 5. Obtaining answers to a few questions   <br />
    (A: Jupyter/Python, B: RStudio, C: Org mode)

+++
## Traditional data analysis

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/traditional-analysis-1.svg" style="width:70%; margin-top: none; border: none; background: none; box-shadow: none;" class="fragment current-visible" />

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/traditional-analysis-2.svg" style="width:70%; margin-top: none; border: none; background: none; box-shadow: none;" class="fragment current-visible" />

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/traditional-analysis-3.svg" style="width:70%; margin-top: none; border: none; background: none; box-shadow: none;" class="fragment current-visible" />

+++
## Replicable data analysis

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/replicable-analysis-1.svg" style="width:70%; margin-top: none; border: none; background: none; box-shadow: none;" class="fragment current-visible" />

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/replicable-analysis-2.svg" style="width:70%; margin-top: none; border: none; background: none; box-shadow: none;" class="fragment current-visible" />

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/replicable-analysis-3.svg" style="width:70%; margin-top: none; border: none; background: none; box-shadow: none;" class="fragment current-visible" />

+++
## Why do it replicably?

 - Easy to re-do if the data change
 - <!-- .element class="fragment" -->Easy to modify
 - <!-- .element class="fragment" -->Easy to inspect and verify

---
# C028AL-M3-S2

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/clap.png" style="width:50%; margin-top: none; border: none; background: none; box-shadow: none;" />

+++
### 3. Diving in: a replicable analysis

 1. What is a replicable analysis?
 2. **Case study: incidence of influenza-like illness**
 3. Data import <br />
    (A: Jupyter/Python, B: RStudio, C: Org mode)
 4. Verification and inspection <br />
    (A: Jupyter/Python, B: RStudio, C: Org mode)
 5. Obtaining answers to a few questions   <br />
    (A: Jupyter/Python, B: RStudio, C: Org mode)

+++
# Demonstration


+++
## Take-home message

- No manual editing of data
- <!-- .element class="fragment" -->Everything is done in code!

---
# C028AL-M3-S3A

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/clap.png" style="width:50%; margin-top: none; border: none; background: none; box-shadow: none;" />

+++
### 3. Diving in: a replicable analysis

 1. What is a replicable analysis?
 2. Case study: incidence of influenza-like illness
 3. **Data import**  <br />
    (**A: Jupyter/Python**, B: RStudio, C: Org mode)
 4. Verification and inspection <br />
    (A: Jupyter/Python, B: RStudio, C: Org mode)
 5. Obtaining answers to a few questions   <br />
    (A: Jupyter/Python, B: RStudio, C: Org mode)

+++
## Technical choices

 - Jupyter notebook
 - Python 3 language  <!-- .element: class="fragment" -->
 - Libraries: <br />
   &nbsp;&nbsp;&nbsp;&nbsp; pandas  <br />
   &nbsp;&nbsp;&nbsp;&nbsp; matplotlib  <br />
   &nbsp;&nbsp;&nbsp;&nbsp; isoweek   <!-- .element: class="fragment" -->

+++
# Demonstration


+++
## Take-home message

 - The data are read directly from the source
 - <!-- .element class="fragment" -->Missing data must be handled

---
# C028AL-M3-S4A

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/clap.png" style="width:50%; margin-top: none; border: none; background: none; box-shadow: none;" />

+++
### 3. Diving in: a replicable analysis

 1. What is a replicable analysis?
 2. Case study: incidence of influenza-like illness
 3. Data import <br />
    (A: Jupyter/Python, B: RStudio, C: Org mode)
 4. **Verification and inspection** <br />
    (**A: Jupyter/Python**, B: RStudio, C: Org mode)
 5. Obtaining answers to a few questions   <br />
    (A: Jupyter/Python, B: RStudio, C: Org mode)

+++
# Demonstration


+++
## Take-home message

- Preprocessing
  + Adapt the data to the software's conventions   <!-- .element: class="fragment" -->
  + Facilitates the analysis   <!-- .element: class="fragment" -->
- Verify as much as possible   <!-- .element: class="fragment" -->
  + Visual inspection   <!-- .element: class="fragment" -->
  + Validation code   <!-- .element: class="fragment" -->

---
# C028AL-M3-S5A

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/clap.png" style="width:50%; margin-top: none; border: none; background: none; box-shadow: none;" />

+++
### 3. Diving in: a replicable analysis

 1. What is a replicable analysis?
 2. Case study: incidence of influenza-like illness
 3. Data import <br />
    (A: Jupyter/Python, B: RStudio, C: Org mode)
 4. Verification and inspection <br />
    (A: Jupyter/Python, B: RStudio, C: Org mode)
 5. **Obtaining answers to a few questions**  <br />
    (**A: Jupyter/Python**, B: RStudio, C: Org mode)

+++

## Questions

 1. Which years have seen the strongest epidemics?
 2. <!-- .element class="fragment" --> What is the frequency of weak, average, and strong epidemics?

+++
# Demonstration

+++
## Take-home message

<p>A replicable analysis must contain  **all** data processing steps <br/>
   in an **executable** form.
</p>

<p class="fragment">It is important to **explain** all choices<br/>
                    that have an impact on the results.
</p>

<p class="fragment">This requires making many **technical details** explicit, <br />
                    because that is where most mistakes happen!
</p>


---
# C028AL-M3-S3B

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/clap.png" style="width:50%; margin-top: none; border: none; background: none; box-shadow: none;" />

+++
### 3. Diving in: a replicable analysis

 1. What is a replicable analysis?
 2. Case study: incidence of influenza-like illness
 3. **Data import**  <br />
    (A: Jupyter/Python, **B: RStudio**, C: Org mode)
 4. Verification and inspection <br />
    (A: Jupyter/Python, B: RStudio, C: Org mode)
 5. Obtaining answers to a few questions   <br />
    (A: Jupyter/Python, B: RStudio, C: Org mode)


+++
## Technical choices

 - RStudio development environment
 - R language  <!-- .element: class="fragment" -->
 - Library: parsedate <!-- .element: class="fragment" -->

+++
# Demonstration

+++
## Take-home message

 - The data are read directly from the source
 - <!-- .element class="fragment" -->Missing data must be handled

---
# C028AL-M3-S4B

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/clap.png" style="width:50%; margin-top: none; border: none; background: none; box-shadow: none;" />

+++
### 3. Diving in: a replicable analysis

 1. What is a replicable analysis?
 2. Case study: incidence of influenza-like illness
 3. Data import <br />
    (A: Jupyter/Python, B: RStudio, C: Org mode)
 4. **Verification and inspection** <br />
    (A: Jupyter/Python, **B: RStudio**, C: Org mode)
 5. Obtaining answers to a few questions   <br />
    (A: Jupyter/Python, B: RStudio, C: Org mode)

+++
# Demonstration


+++
## Take-home message

- Preprocessing
  + Adapt the data to the software's conventions   <!-- .element: class="fragment" -->
  + Facilitates the analysis   <!-- .element: class="fragment" -->
- Verify as much as possible   <!-- .element: class="fragment" -->
  + Visual inspection   <!-- .element: class="fragment" -->
  + Validation code   <!-- .element: class="fragment" -->

---
# C028AL-M3-S5B

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/clap.png" style="width:50%; margin-top: none; border: none; background: none; box-shadow: none;" />

+++
### 3. Diving in: a replicable analysis

 1. What is a replicable analysis?
 2. Case study: incidence of influenza-like illness
 3. Data import <br />
    (A: Jupyter/Python, B: RStudio, C: Org mode)
 4. Verification and inspection <br />
    (A: Jupyter/Python, B: RStudio, C: Org mode)
 5. **Obtaining answers to a few questions**  <br />
    (A: Jupyter/Python, **B: RStudio**, C: Org mode)

+++

## Questions

 1. Which years have seen the strongest epidemics?
 2. <!-- .element class="fragment" --> What is the frequency of weak, average, and strong epidemics?

+++

## Demonstration

 
+++
## Take-home message

<p>A replicable analysis must contain  **all** data processing steps <br/>
   in an **executable** form.
</p>

<p class="fragment">It is important to **explain** all choices<br/>
                    that have an impact on the results.
</p>

<p class="fragment">This requires making many **technical details** explicit, <br />
                    because that is where most mistakes happen!
</p>

---
# C028AL-M3-S3C

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/clap.png" style="width:50%; margin-top: none; border: none; background: none; box-shadow: none;" />

+++
### 3. Diving in: a replicable analysis

 1. What is a replicable analysis?
 2. Case study: incidence of influenza-like illness
 3. **Data import**  <br />
    (A: Jupyter/Python, B: RStudio, **C: Org mode**)
 4. Verification and inspection <br />
    (A: Jupyter/Python, B: RStudio, C: Org mode)
 5. Obtaining answers to a few questions   <br />
    (A: Jupyter/Python, B: RStudio, C: Org mode)

+++
## Technical choices

 - Emacs editor + Org mode
 - Languages: <br />
   &nbsp;&nbsp;&nbsp;&nbsp; Python 3 for pre-processing <br />
   &nbsp;&nbsp;&nbsp;&nbsp; R for analysis <!-- .element: class="fragment" -->

+++
# Demonstration

  
+++
## Take-home message

 - The data are read directly from the source
 - <!-- .element class="fragment" -->Missing data must be handled

---
# C028AL-M3-S4C

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/clap.png" style="width:50%; margin-top: none; border: none; background: none; box-shadow: none;" />

+++
### 3. Diving in: a replicable analysis

 1. What is a replicable analysis?
 2. Case study: incidence of influenza-like illness
 3. Data import <br />
    (A: Jupyter/Python, B: RStudio, C: Org mode)
 4. **Verification and inspection** <br />
    (A: Jupyter/Python, B: RStudio, **C: Org mode**)
 5. Obtaining answers to a few questions   <br />
    (A: Jupyter/Python, B: RStudio, C: Org mode)

+++
# Demonstration

+++
## Take-home message

- Preprocessing
  + Adapt the data to the software's conventions   <!-- .element: class="fragment" -->
  + Facilitates the analysis   <!-- .element: class="fragment" -->
- Verify as much as possible   <!-- .element: class="fragment" -->
  + Visual inspection   <!-- .element: class="fragment" -->
  + Validation code   <!-- .element: class="fragment" -->

---
# C028AL-M3-S5C

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/clap.png" style="width:50%; margin-top: none; border: none; background: none; box-shadow: none;" />

+++
### 3. Diving in: a replicable analysis

 1. What is a replicable analysis?
 2. Case study: incidence of influenza-like illness
 3. Data import <br />
    (A: Jupyter/Python, B: RStudio, C: Org mode)
 4. Verification and inspection <br />
    (A: Jupyter/Python, B: RStudio, C: Org mode)
 5. **Obtaining answers to a few questions**  <br />
    (A: Jupyter/Python, B: RStudio, **C: Org mode**)

+++

## Questions

 1. Which years have seen the strongest epidemics?
 2. <!-- .element class="fragment" --> What is the frequency of weak, average, and strong epidemics?

+++
# Demonstration

+++
## Take-home message

<p>A replicable analysis must contain  **all** data processing steps <br/>
   in an **executable** form.
</p>

<p class="fragment">It is important to **explain** all choices<br/>
                    that have an impact on the results.
</p>

<p class="fragment">This requires making many **technical details** explicit, <br />
                    because that is where most mistakes happen!
</p>
