---
C028AL-W2-S1 
===========================================

+++
Vers une étude reproductible : la réalité du terrain
----------------------------------------------------

**Module 4. Vers une étude reproductible : la réalité du terrain**

1.  **L'enfer des données**
2.  L'enfer du logiciel
3.  L'enfer du calcul
4.  Conclusion

+++
## Deux « nouveaux » problèmes

Lorsque nous commençons à travailler sur de « vraies » données nous nous trouvons généralement confrontés à deux problèmes : 
- les données sont de nature « diverse »
- les données occupent un grand espace mémoire

+++
## Les données « non homogènes »

- Les données grippales du module 3 se prêtent bien à une présentation en table (objet à 2 dimensions)
- Souvent la forme table doit être *abandonnée* car : 
  - les colonnes n'ont pas la même longueur
  - les données peuvent être des suites chronologiques **et** des images, etc.
  
+++
## Les données sont « trop grosses »

- **Format texte** pas toujours approprié pour des nombres
- Choix d'un **format binaire** car :
  - Les nombres prennent moins de place mémoire
  - Nombres en format texte ⇒ conversion en binaire pour calculs.

+++
## Ce qu'il faut garder du format texte : les métadonnées

- Le format texte permet de stocker les données **et** tout le reste...
- ⇒ ajouter des informations sur les données :
  - provenance
  - date d'enregistrement
  - source
  - etc.
  
+++


- Ces informations sur les données sont ce qu'on appelle les **métadonnées**
- Elles sont vitales pour la mise en œuvre de la recherche reproductible.

+++
## Ce qu'il faut garder du format texte : le boutisme

- Format texte « universel »
- Formats binaires dépendent de l'architecture ou de l'OS
- 1010, codé sur 4 bits peut être lu comme :
  - 1x1 + 0x2 + 1x4 + 0x8 = 5, c'est le *petit-boutisme*
  - 1x8 + 0x4 + 1x2 + 0x1 = 10, c'est le *gros-boutisme*
- **Un stockage binaire pour la recherche reproductible doit spécifier le boutisme**.

+++
## Des formats binaires, pour données composites, permettant le sauvegarde de métadonnées

Rechercher des formats binaires pour :
- travailler avec de grosses données de natures différentes
- stocker des métadonnées avec les données
- avoir un boutisme fixé **une fois pour toute**

+++
## `FITS` et `HDF5`

- le *Flexible Image Transport System* (`FITS`), créé en 1981 est toujours régulièrement mis à jour
- le *Hierarchical Data Format* (`HDF`), développé au *National Center for Supercomputing Applications*, il en est à sa cinquième version, `HDF5`

+++
## `FITS`

- `FITS` introduit et mis à jour par les astrophysiciens
- Format suffisamment général pour utilisation dans différents contextes

+++
## Anatomie d'un fichier `FITS`

- 1 ou plusieurs segments : *Header/Data Units* (HDUs)
- HDU constituée :
  - d'une en-tête (*Header Unit*) suivie, *mais ce n'est pas obligatoire*, par
  - des données (*Data Unit*)
- En-tête = paires mots clés / valeurs  → **métadonnées**
- Données tableaux binaires (une à 999 dimensions) ou tables (texte ou binaire).

+++
## Manipulation des fichiers `FITS`

- Les développeurs du format fournissent une bibliothèque `C` et des programmes associés (faciles à utiliser).
- Les utilisateurs de `Python` pourront utiliser `PyFITS`
- Les utilisateurs de `R` pourront  utiliser `FITSio`.

+++
## `HDF5`

- Organisation hiérarchique, ressemble à une arborisation de fichiers.
- Élément structurant : le *group* (répertoire), il contient un ou plusieurs *datasets* (jeux de données).
- Les *groups* peuvent être imbriqués.
- Les métadonnées n'ont pas de structure imposée.
- Les données n'ont pas de structure imposée — elle peuvent être des textes. 

+++
## Manipulation des fichiers `HDF5`

- Format plus souple ⇒ bibliothèque `C` plus compliquée que son équivalent `FITS`.
- La bibliothèque distribuée avec `HDFView` : outil puissant d'exploration et de visualisation.
- `Python` dispose d'une interface très complète avec `h5py`.
- Il y a trois paquets `R` : `h5`, `hdf5r` et `rhdf5`.

+++
## Conclusions

- Vraies données ⇒ problèmes de taille et de structure.
- Vraies données complexes ⇒ métadonnées.
- `FITS` et `HDF5` = solutions **pratiques**.
- En complexité et flexibilité : `FITS` < `HDF5`.
- Plates-formes d'archivage ⇒ stockage pérenne accessible à tous.
