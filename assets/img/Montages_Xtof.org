#+STARTUP: indent
* Module 1 deuxième partie
Source et instructions pour générer les figures

** Figure introductive historique

- https://commons.wikimedia.org/wiki/File:Clay_Tablet_-_Louvre_-_AO29562.jpg#/media/File:Clay_Tablet_-_Louvre_-_AO29562.jpg ;
- https://commons.wikimedia.org/wiki/File:Meister_des_Portr%C3%A4ts_des_Paquius_Proculus_001.jpg#/media/File:Meister_des_Portr%C3%A4ts_des_Paquius_Proculus_001.jpg ;
- https://commons.wikimedia.org/wiki/File:Commonplace_book_mid_17th_century.jpg#/media/File:Commonplace_book_mid_17th_century.jpg
- https://commons.wikimedia.org/wiki/File:Lhfhospitalsstatehospital001.jpg#/media/File:Lhfhospitalsstatehospital001.jpg
- https://commons.wikimedia.org/wiki/File:Fait_cnv.jpg#/media/File:Fait_cnv.jpg
- https://commons.wikimedia.org/wiki/File:Lenovo_Yoga_3_Pro.jpg#/media/File:Lenovo_Yoga_3_Pro.jpg

#+BEGIN_SRC bash
wget https://upload.wikimedia.org/wikipedia/commons/thumb/e/e2/Clay_Tablet_-_Louvre_-_AO29562.jpg/1024px-Clay_Tablet_-_Louvre_-_AO29562.jpg -O Tablette_argile.jpg
wget https://upload.wikimedia.org/wikipedia/commons/thumb/8/85/Meister_des_Portr%C3%A4ts_des_Paquius_Proculus_001.jpg/651px-Meister_des_Portr%C3%A4ts_des_Paquius_Proculus_001.jpg -O Proculus.jpg
wget https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Commonplace_book_mid_17th_century.jpg/878px-Commonplace_book_mid_17th_century.jpg -O Carnet.jpg
wget https://upload.wikimedia.org/wikipedia/commons/6/6e/Lhfhospitalsstatehospital001.jpg -O Carte.jpg
wget https://upload.wikimedia.org/wikipedia/commons/e/ef/Fait_cnv.jpg -O Post_it.jpg
wget https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Lenovo_Yoga_3_Pro.jpg/1024px-Lenovo_Yoga_3_Pro.jpg -O Tablette_ordi.jpg
montage -mode concatenate -tile 3x2 Tablette_argile.jpg Proculus.jpg Carnet.jpg Carte.jpg Post_it.jpg Tablette_ordi.jpg Figure_W1_S2_1.jpg
#+END_SRC

#+RESULTS:

** Figure Tablette de cire et Stylet
Source : site de Jacques Poitou (http://j.poitou.free.fr/pro/img-p/tkn/tabula.html)

#+BEGIN_SRC bash
wget http://j.poitou.free.fr/pro/img-p/tkn/tabula.jpg
wget http://j.poitou.free.fr/pro/img-p/tkn/stilus.jpg
convert tabula.jpg stilus.jpg +append tabula_.jpg
convert tabula_.jpg -font FreeSans -pointsize 35 -gravity southeast -annotate 0 'Musée romain-germanique\nCologne (Allemagne)\nPhotos de Jacques Poitou' tabula_stilus.jpg 
#+END_SRC

** Figure du Volumen au codex

- la frise chronologique est une capture d'écran de la [[https://fr.wikipedia.org/wiki/Codex][page wikipédia sur le codex]] (la capture est stockée dans un fichier nommé =frise_chronologique.jpg=) ;
- la mosaïque avec les volumen, Clio, Virgile et Melpomène : https://commons.wikimedia.org/wiki/File:A_mosaic_BardoMuseum.JPG#/media/File:A_mosaic_BardoMuseum.JPG ;
- L'illustration du codex : https://commons.wikimedia.org/wiki/File:CODICOLOGY.jpg#/media/File:CODICOLOGY.jpg ;

#+BEGIN_SRC bash
wget https://upload.wikimedia.org/wikipedia/commons/3/37/CODICOLOGY.jpg
wget https://upload.wikimedia.org/wikipedia/commons/thumb/1/15/A_mosaic_BardoMuseum.JPG/640px-A_mosaic_BardoMuseum.JPG -O Clio_Virgile_Melpomene.jpg
convert Clio_Virgile_Melpomene.jpg CODICOLOGY.jpg +append Volumen_codex.jpg
convert frise_chronologique.jpg Volumen_codex.jpg -append Figure_W1_S2_3.jpg
#+END_SRC

** Image D'Eusèbe

#+BEGIN_SRC bash 
wget https://upload.wikimedia.org/wikipedia/commons/b/ba/Eusebius_of_Caesarea.jpg
wget https://upload.wikimedia.org/wikipedia/commons/1/1f/Romia_Imperio.png
convert Romia_Imperio.png Eusebius_of_Caesarea.jpg -gravity northeast -composite Eusebius.jpg
convert Eusebius.jpg -font FreeSans -pointsize 75 -gravity southwest -annotate 0 'Empire romain' Eusebius_leg.jpg
convert Eusebius_leg.jpg -font FreeSans -pointsize 75 -annotate +700+75 'Eusèbe de\nCésarèe\n(265-340)' Eusebius_leg2.jpg
convert Eusebius_leg2.jpg -font FreeSans -pointsize 30 -annotate +1150+900 'Césarèe' Eusebe_final.jpg
#+END_SRC

** Canon eusébien

- https://commons.wikimedia.org/wiki/File:Codexaureus_05.jpg#/media/File:Codexaureus_05.jpg ;
- https://commons.wikimedia.org/wiki/File:Codexaureus_07.jpg#/media/File:Codexaureus_07.jpg.

#+BEGIN_SRC bash
wget https://upload.wikimedia.org/wikipedia/commons/4/47/Codexaureus_05.jpg
wget https://upload.wikimedia.org/wikipedia/commons/a/ad/Codexaureus_07.jpg
convert Codexaureus_05.jpg Codexaureus_07.jpg +append Codex_eusebien.jpg
convert Codex_eusebien.jpg -font FreeSans -pointsize 35 -gravity south -annotate 0 'Évangéliaire de Lorsch (778-820)' Evangeliaire_Lorsch.jpg 
#+END_SRC

** Leishu

- Yongle Dadian, le plus grand Leishu jamais compilé (1408, contient 370x10$^{6}$ caractères chinois, https://commons.wikimedia.org/wiki/File:Yongle_Dadian_Encyclopedia_1403.jpg#/media/File:Yongle_Dadian_Encyclopedia_1403.jpg ;
- Matrice d'impression de billets de banque (dynastie Song du Nord 960-1127) : https://commons.wikimedia.org/wiki/File:Beijing.China_printing_museum.Plate_of_Paper_money.Northern_Song_Dynasty.jpg#/media/File:Beijing.China_printing_museum.Plate_of_Paper_money.Northern_Song_Dynasty.jpg

#+BEGIN_SRC bash
wget https://upload.wikimedia.org/wikipedia/commons/e/e5/Yongle_Dadian_Encyclopedia_1403.jpg
wget https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Beijing.China_printing_museum.Plate_of_Paper_money.Northern_Song_Dynasty.jpg/615px-Beijing.China_printing_museum.Plate_of_Paper_money.Northern_Song_Dynasty.jpg -O Matrice_billet_song.jpg
convert Yongle_Dadian_Encyclopedia_1403.jpg Matrice_billet_song.jpg +append -font FreeSans -pointsize 30 -gravity southwest -annotate +25-150 "En haut : leishu Yongle Dadian\n(1403) 370 millions de caractères.\nÀ droite : matrice d'impression\nde billets de banque\nDynastie Song du Nord (960-1127)." Figure_W1_S2_6.jpg 
#+END_SRC

** Figures suivantes
- Nouvelle présentation de l'armoire Placcius / Leibniz
- Méthode d'indexation de Locke illustrée sur mon cahier de notes 1 & 2
* Module 1 quatrième partie
** Exemple manuscrit raturé de Laclos
Une capture d'écran de la page 258 de :
#+BEGIN_EXAMPLE
Date d'édition : 
    1701-1900 
Contributeur : 
    Pierre-Ambroise Choderlos de Laclos 
Type : 
    manuscrit 
Langue : 
    français 
Format : 
    Papier. - 143 ff. - 240 x 190mm. - Demi-reliure chagrin bleu 
Description : 
    Manuscrit autographe, état intermédiaire corrigé, vers 1779-1781 aux f. 35-127. Précédé de lettres de Mme Riccoboni 
Droits : 
    domaine public 
Identifiant : 
    ark:/12148/btv1b60002397 
Source : 
    Bibliothèque nationale de France, Département des manuscrits, Français 12845 
Provenance : 
    Bibliothèque nationale de France 
Date de mise en ligne : 
    09/10/2009 
#+END_EXAMPLE

** La gestion de version « à la Git »
Le schéma de [[https://git-scm.com/book/fr/v2/D%C3%A9marrage-rapide-Rudiments-de-Git][ProGit2]] (sous licence CC) sur les instantanés :
#+BEGIN_SRC bash
wget https://git-scm.com/book/en/v2/images/snapshots.png
#+END_SRC

Celui sur la gestion distribuée :
#+BEGIN_SRC bash
wget https://git-scm.com/book/en/v2/images/distributed.png
#+END_SRC
