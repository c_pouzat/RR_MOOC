# Quelques idées pour démarrer

Tout d'abord, la source de ma citation de Marc Bloch en faveur, dans notre cas, de l'usage du français plutôt que de l'anglais est son bouquin « [La société féodale](http://classiques.uqac.ca/classiques/bloch_marc/societe_feodale/societe_feodale.html) » (p 84 le dernier paragraphe de la section II du chapitre II) :

	Ainsi la langue technique du droit elle-même ne disposait que d’un
	vocabulaire à la fois trop archaïque et trop flottant pour lui permettre de serrer
	de près la réalité. Quant au lexique des parlers usuels, il avait toute
	l’imprécision et l’instabilité d’une nomenclature purement orale et populaire.
	Or, en matière d’institutions sociales, le désordre des mots entraîne presque
	nécessairement celui des choses. Ne fût-ce qu’en raison de l’imperfection de
	leur terminologie, une grande incertitude pesait donc sur le classement des
	rapports humains. Mais l’observation doit être encore élargie. A quelque
	usage qu’on l’appliquât, le latin avait l’avantage d’offrir, aux intellectuels de
	l’époque, un moyen de communication international. Il présentait, par contre,
	le redoutable inconvénient d’être, chez la plupart des hommes qui s’en
	servaient, radicalement séparé de la parole intérieure ; de les contraindre, par
	suite, dans l’énonciation de leur pensée, à de perpétuels à peu près. L’absence
	d’exactitude mentale, qui fut, nous l’avons vu, une des caractéristiques de ce
	temps, comment, parmi les causes multiples qui sans doute conspirent à
	l’expliquer, ne pas ranger ce va-et-vient incessant entre les deux plans du
	langage ?

## Un plan ou un contenu ?

J'aurais tendance à suggérer un découpage en trois parties (inégales) :

1. historique de la RR ;
2. le cas « simple » du cahier de labo ou de la présentation -- un seul outil,`R` ou `Python` impliqué -- ;
3. le cas « grandeur nature » de l'article ou du rapport -- plusieurs outils impliqués --.

Le point 2 nous amènerait à présenter les langages de balisage léger en insistant sur `markdown / pandoc` ainsi que les logiciels de contrôle de version.

Le point 3, où en plus de `R` ou `Python` il faudrait du code compilé et des données récupérées sur un dépôt,  serait l'occasion de discuter : 

- des dépôts de données type [zenodo](https://zenodo.org/) ; 
- des formats « flexibles » de données ([HDF5](https://support.hdfgroup.org/HDF5/), [FITS](https://fits.gsfc.nasa.gov/), etc.) ;
- des moteurs de production (`make`, `scons`) ;
- des logiciels de virtualisation ([Docker](https://www.docker.com/)).

## Pour motiver les étudiants

On pourrait proposer aux plus motivés de choisir une article ou une partie d'un « gros » article, de le/la reproduire et de soumettre à [ReScience](https://rescience.github.io/).

## Une base possible ? pour le début au moins

Notre (Konrad, Andrew Davison et moi) papier « [La recherche reproductible : une communication scientifique explicite](http://publications-sfds.fr/index.php/stat_soc/article/view/448) » dans _Statistique et Société_ me semble consituter un « bon » point de départ (pour les points 1 et 2), surtout avec le site associé : <https://github.com/khinsen/article-statistique-et-societe>. 

# Sur le « cas Méchain et Delambre »

Le bouquin de Ken Alder a été traduit en français (et est disponible en format poche) sous le titre « Mersurer le monde -- L'incroyable histoire de l'invention du mètre ». L'auteur y fait explicitement référence aux « cahiers d'observations / de terrain » de Méchain et Delambre et il doit citer ses sources (j'ai prété ma copie de la version originale du bouquin à un copain qui a oublié de me la rendre ; je vais donc en commander une version française en poche pour voir si ces sources ne seraient pas facilement accessibles). Si on peut présenter des extraits des dits cahiers, on pourrait potentiellement faire une « chouette » ensemble avec une petit exemple concret de RR, puisque nous pourrions reprendre l'exemple de la méthode des moindres carrées de Legendre appliquées à ces données. Le premier chapitre du bouquin de Stephen Stigler _The History of Statistics: The Measurement of Uncertainty Before 1900_, contient une discussion de la méthode des moindres carrés et des données de Méchain et Delambre. L'appendice de Legendre sur les moindres carrés est disponibles sur Gallica et sur le [site binum](http://www.bibnum.education.fr/math%c3%a9matiques/alg%c3%a9bre/legendre-et-la-m%c3%a9thode-des-moindres-carr%c3%a9s).

# Un (autre) petit exemple possible
En plus des données d'origine « économique » qui devraient être « parlantes » pour une majorité de nos auditeurs, un cas d'épidémie pourrait aussi être considéré. Nous pourrions alors utiliser le court papier de David Kendall [_An Artificial Realization of a Simple "Birth-and-Death" Process_](http://www.jstor.org/stable/2983837?seq=1#page_scan_tab_contents), où, en 1950 (!) il utilise la méthode maintenant connue sous le nom de « méthode de Gillespie », 26 ans avant son introduction en chimie (et en ayant en plus la preuve de sa validité). Je vous envoie le papier en PDF (la méthode de simulation « à la main » est aussi remarquable que savoureuse).
