# Souhaits de développements pour la plate-forme FUN

Point de départ: intégration de notebooks IPython pour le MOOC Bioinfo (comme cet [exemple](https://www.fun-mooc.fr/courses/course-v1:inria+41007+session02/courseware/25b76ca08ab846e4bef0569cf556f401/95d35d6181844d1b8053f45110f945cc/))

## Mise à jour / extensions

 - IPython -> Jupyter
 - Python 2 -> Python 3
 - Support pour le langage R
 
## Gestion de versions

 - Possibilité pour l'utilisateur de sauvegarder des versions multiples de ses notebooks

 - Stockage dans un dépôt git
    + sans exposer l'utilisateur directement à l'interface en ligne de commande de git
    + plutôt un bouton "sauvegarder cette version" et une façon d'accéder à l'historique
    + la cerise sur le gâteau: comparaison des versions par [nbdime](https://github.com/jupyter/nbdime)

 - Stockage des dépôt git sur un serveur extérieur (GitLab, ...)
   + pérennité au-delà du MOOC
   + possibilité d'utiliser les "outils de pro" sur ces données
