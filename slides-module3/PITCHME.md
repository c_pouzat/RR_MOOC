# C028AL-M3-S1

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/clap.png" style="width:50%; margin-top: none; border: none; background: none; box-shadow: none;" />

+++
### 3. La main à la pâte&emsp14;: une analyse réplicable

 1. **Une analyse réplicable, c'est quoi&emsp14;?**
 2. Étude de cas&emsp14;: l'incidence de syndromes grippaux
 3. Importer les données <br />
    (A&emsp14;: Jupyter/Python, B&emsp14;: RStudio, C&emsp14;: Org mode)
 4. Vérification et inspection <br />
    (A&emsp14;: Jupyter/Python, B&emsp14;: RStudio, C&emsp14;: Org mode)
 5. Questions et réponses   <br />
    (A&emsp14;: Jupyter/Python, B&emsp14;: RStudio, C&emsp14;: Org mode)

Note:
  Dans ce module, nous vous montrons pas par pas la construction d'une
  analyse de données réplicable sous forme d'un document
  computationnel. Mais d'abord nous vous expliquons ce que c'est
  exactement, une analyse de données réplicable.

+++
## L'analyse de données traditionnelle

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/analyse-traditionnelle-1.svg" style="width:70%; margin-top: none; border: none; background: none; box-shadow: none;" class="fragment current-visible" />

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/analyse-traditionnelle-2.svg" style="width:70%; margin-top: none; border: none; background: none; box-shadow: none;" class="fragment current-visible" />

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/analyse-traditionnelle-3.svg" style="width:70%; margin-top: none; border: none; background: none; box-shadow: none;" class="fragment current-visible" />

Note:
  Dans une analyse de données traditionnelle, on se concentre sur les
  résultats,
  
  en fournissant seulement un résumé méthodologique des
  calculs qui ont permis de les obtenir.
  
  On termine avec une discussion, par exemple pour exposer les
  conclusions qu'on tire des résultats.

+++
## L'analyse de données réplicable

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/analyse-replicable-1.svg" style="width:70%; margin-top: none; border: none; background: none; box-shadow: none;" class="fragment current-visible" />

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/analyse-replicable-2.svg" style="width:70%; margin-top: none; border: none; background: none; box-shadow: none;" class="fragment current-visible" />

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/analyse-replicable-3.svg" style="width:70%; margin-top: none; border: none; background: none; box-shadow: none;" class="fragment current-visible" />

Note:
  Une analyse réplicable remplace le résumé méthodologique par la
  totalité du code qui a été utilisé pour faire les calculs.
  
  On l'accompagne d'une explication détaillée, qui documente notamment
  les choix qui ont été faits à chaque étape.

+++
## Pourquoi faire réplicable ?

 - Facile à refaire si les données changent 
 - <!-- .element class="fragment" -->Facile à modifier 
 - <!-- .element class="fragment" -->Facile à inspecter et vérifier

Note:
  Une analyse réplicable a plusieurs avantages, autant pour son auteur
  que pour ses lecteurs.

  Premièrement, si les données changent dans l'avenir, il est facile
  de mettre à jour leur analyse.

  Deuxièmement, l'analyse elle-même est facile à modifier, quand des
  nouvelles questions se posent.

  Troisièmement, un lecteur peut mieux comprendre l'analyse s'il peut
  inspecter le code mais aussi des résultats intermédiaires des
  calculs. On fait plus facilement confiance à une analyse qui est
  vérifiable en tout détail, en commençant par les données brutes.

---
# C028AL-M3-S2

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/clap.png" style="width:50%; margin-top: none; border: none; background: none; box-shadow: none;" />

+++
### 3. La main à la pâte&emsp14;: une analyse réplicable

 1. Une analyse réplicable, c'est quoi&emsp14;?
 2. **Étude de cas&emsp14;: l'incidence de syndromes grippaux**
 3. Importer les données <br />
    (A&emsp14;: Jupyter/Python, B&emsp14;: RStudio, C&emsp14;: Org mode)
 4. Vérification et inspection <br />
    (A&emsp14;: Jupyter/Python, B&emsp14;: RStudio, C&emsp14;: Org mode)
 5. Questions et réponses   <br />
    (A&emsp14;: Jupyter/Python, B&emsp14;: RStudio, C&emsp14;: Org mode)

Note:
  Nous allons maintenant analyser des données qui proviennent du
  "Réseau Sentinelles". Ce réseau regroupe des médecins généralistes
  qui surveillent la fréquence de certaines maladies tout au long de
  l'année.

+++
# Démo

Note:
  Sur le site du Réseau Sentinelles, nous allons à la base de données,
  ou nous sélectionnons l'incidence de syndromes grippaux. Puis nous
  choisissons le téléchargement en format CSV des des données depuis
  le début des observations, en 1985, pour la France Métropolitaine.

  Voici le fichier que nous avons téléchargé. C'est un fichier texte
  en format CSV, ce qui veut dire "comma-separated values", ou valeurs
  séparées par des virgules. Il s'agit d'un format très populaire,
  mais malheureusement peu standardisé.

  La première ligne n'est qu'un commentaire, sans importance pour
  l'interprétation des données. La deuxième ligne contient les
  étiquettes pour les colonnes. Les autres lignes, une par semaine,
  contiennent les données.

  Il y a deux colonnes qui nous intéressent:
  "week" donne l'année et le numéro de la semaine suivant la
  convention ISO (Organisation internationale de normalisation), qui
  prévoit que la semaine N°1 de chaque année soit celle qui contient
  le 4 janvier.
  "inc" donne l'incidence de la maladie, définie comme le nombre de
  consultations chez les médecins généralistes lors desquelles la
  maladie a été diagnostiquée.

  Nous allons maintenant parcourir notre fichier pour voir s'il y a
  des anomalies. Pour l'instant tout se passe bien... ... mais là,
  pour la semaine 19 de l'année 1989, il n'y a rien, pas de données.
  C'est une situation très fréquente dans l'analyse de données d'observation.
  Suite à des erreurs diverses et variées, il y a des données manquantes,
  dont il faut tenir compte dans l'analyse.

  Il est tentant de supprimer la ligne des données manquantes avec un
  éditeur de texte, puis analyser ce fichier modifié sans se soucier
  du problème déjà résolu.

  Pourtant, une telle approche n'est pas réplicable. Dans le fichier
  des données, il ne resterait aucune trace de la suppression d'un
  point. Un lecteur de l'analyse ne saurait jamais qu'un point a été
  supprimé, et pour quelle raison. Au pire, un lecteur qui compare
  avec les données d'origine pourrait penser à une erreur ou à une
  tentative de fraude.

+++
## Les points clés à retenir

- Aucune modification des données "à la main".
- <!-- .element class="fragment" -->Du code pour tout&emsp14;!

Note:
  Nous devons donc nous abstenir de toute modification des données à la main,

  et écrire du code pour toute manipulation.

  Dans la prochaine séquence nous vous montrons pas par pas comment
  faire cela.

---
# C028AL-M3-S3A

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/clap.png" style="width:50%; margin-top: none; border: none; background: none; box-shadow: none;" />

+++
### 3. La main à la pâte&emsp14;: une analyse réplicable

 1. Une analyse réplicable, c'est quoi&emsp14;?
 2. Étude de cas&emsp14;: l'incidence de syndromes grippaux
 3. **Importer les données**  <br />
    (**A&emsp14;: Jupyter/Python**, B&emsp14;: RStudio, C&emsp14;: Org mode)
 4. Vérification et inspection <br />
    (A&emsp14;: Jupyter/Python, B&emsp14;: RStudio, C&emsp14;: Org mode)
 5. Questions et réponses   <br />
    (A&emsp14;: Jupyter/Python, B&emsp14;: RStudio, C&emsp14;: Org mode)

Note:
  Dans cette séquence, nous montrons comment créer un document
  computationnel et y importer nos données sur l'incidence du syndrome
  grippal.


+++
## Choix techniques

 - Notebook Jupyter
 - Langage Python 3   <!-- .element: class="fragment" -->
 - Bibliothèques&emsp14;: <br />
   &nbsp;&nbsp;&nbsp;&nbsp; pandas  <br />
   &nbsp;&nbsp;&nbsp;&nbsp; matplotlib  <br />
   &nbsp;&nbsp;&nbsp;&nbsp; isoweek   <!-- .element: class="fragment" -->

Note:
  Nous devons d'abord faire quelques choix techniques:
  - Notre document computationnel est un notebook Jupyter
  - Le langage de programmation est Python dans sa version 3
  - Nous utilisons les bibliothèques
    + pandas pour le traitement des données
    + matplotlib pour générer des plots
    + isoweek pour la gestion des numéros de semaines ISO que pandas ne connaît pas

+++
# Démo

Note:
  Dans la page d'accueil de Jupyter, nous choisissons le langage
  "Python 3". Puis nous donnons un nom à ce notebook, par exemple
  'analyse-syndrome-grippal'.

  Un notebook est une suite de cellules, dont chacune peut contenir du
  code en Python ou du texte en format Markdown. Nous commençons par
  une cellule texte, pour donner un titre à notre document.

  Par la suite, nous demandons à Jupyter d'inclure les graphiques
  créés avec matplotlib directement dans le notebook.

  Il est d'usage de commencer une analyse de données avec
  l'importation de toutes les bibliothèques qu'il utilise, pour que ce
  choix soit immédiatement visible au lecteur.

  Pandas peut lire directement le format CSV. Pour cela nous avons
  besoin de l'URL de téléchargement de nos données. Nous l'obtenons de
  l'historique du navigateur que nous avons utilisé pour télécharger
  le fichier précédemment.

  Il faut encore rajouter le paramètre `skiprows=1`, qui indique à
  Pandas d'ignorer la première ligne du fichier, qui contient un
  commentaire.

  Nous pouvons maintenant jeter un coup d'oeil sur nos données. Pandas
  nous montre les 30 premières et les 30 dernières lignes. Pour
  l'instant, il semble que tout s'est bien passé.

  Nous allons maintenant traiter les données manquantes.

  Voici ce que pandas nous donne quand on demande s'il y a quelque
  part des données manquantes. Malheureusement, la réponse est tout
  aussi grande que le jeux de données lui-même. Nous allons un petit
  peu simplifier en demandant seulement les lignes dans lesquelles il
  y a au moins une valeur manquante. Mais ce sont encore 1700 réponses
  du type oui/non.

  Ce qui nous souhaitons vraiment, c'est de voir les données qui
  correspondent à ces lignes. Et voilà, nous retrouvons la semaine 19
  de l'année 1989.

  Nous éliminons les données manquantes, et nous faisons une copie du
  jeux de données résultants. La nécessité de faire une copie est liée
  à la façon dont Pandas gère la mémoire. Vous trouvez une explication
  dans le manuel de pandas.

  Nous regardons maintenant les données nettoyées. Elles ressemblent
  beaucoup aux données brutes, il y a juste un point en moins !

+++
## Les points clés à retenir

 - Lecture des données directement de la source
 - <!-- .element class="fragment" -->Gestion des données manquantes

Note:
  Nous rappelons les points clés à retenir de cette séquence.

  Premièrement, il faut lire les données directement de la source.

  Deuxièmement, il faut faire attention aux données manquantes.

---
# C028AL-M3-S4A

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/clap.png" style="width:50%; margin-top: none; border: none; background: none; box-shadow: none;" />

+++
### 3. La main à la pâte&emsp14;: une analyse réplicable

 1. Une analyse réplicable, c'est quoi&emsp14;?
 2. Étude de cas&emsp14;: l'incidence de syndromes grippaux
 3. Importer les données <br />
    (A&emsp14;: Jupyter/Python, B&emsp14;: RStudio, C&emsp14;: Org mode)
 4. **Vérification et inspection** <br />
    (**A&emsp14;: Jupyter/Python**, B&emsp14;: RStudio, C&emsp14;: Org mode)
 5. Questions et réponses   <br />
    (A&emsp14;: Jupyter/Python, B&emsp14;: RStudio, C&emsp14;: Org mode)

Note:
  Dans cette séquence, nous préparons les données pour notre analyse.
  Ceci implique notamment de vérifier et inspecter les données,
  pour détecter des anomalies et des erreurs dans l'importation.

+++
# Démo

Note:

  Pour pouvoir travailler avec nos données, nous devons modifier
  la représentation des dates, parce que pandas ne comprend pas le
  format ISO utilisé par le Réseau Sentinelles. Nous montrons cette
  conversion d'abord pour une seule valeur.

  D'abord nous convertissons la valeur initiale en chaîne de caractères.
  Puis nous pouvons extraire séparément l'année et la semaine.
  Nous vérifions que nous avons bien obtenu des valeurs raisonnables,
  ce qui semble bien être le cas.
  
  Le module isoweek nous permet de trouver les dates qui correspondent
  à cette semaine. Nous pouvons maintenant construire un objet Pandas qui
  correspond à la même semaine.
  
  Nous devons appliquer cette conversion à toutes les semaines de notre jeu
  de données. Pour nous faciliter cette tâche, nous en faisons une fonction
  Python.

  Maintenant nous pouvons appliquer cette fonction à toutes les semaines..
  .. et nous avons un jeu de données utilisable par Pandas.

  Pour notre analyse, il est plus pratique de travailler avec une
  serie d'observations dans l'ordre chronologique, donc nous
  appliquons un tri.

  Et voici nos données triées, qui commencent en 1985 et se terminent
  en 2017.

  Il est toujours conseillé de vérifier autant que possible la
  cohérence des données à chaque étape du traitement. Ici nous
  vérifions que chaque période de notre jeu de données commence juste
  après la fin de la période précédente.

  Ceci nécessite une boucle sur toutes les paires de périodes
  adjacentes. Nous calculons la distance temporelle entre le début de
  la deuxième période et la fin de la première. Si elle excède une
  tolérance de 1s, nous affichons les deux périodes pour inspection.

  Nous constatons un seul trou dans notre axe de temps, qui correspond
  à la semaine manquante en 1989.

  Maintenant nous devrions pouvoir regarder nos données sur un plot...
  ... mais ça se passe mal !

  Il semble que nos données d'incidence ne soient pas numeriques ! Voyons...

  En effet, ce sont des chaînes de caractères, pas des entiers !

  Pour comprendre ce qui s'est passé, regardons de nouveau nos données
  d'origine. Pour la semaine 19 de l'année 1989, il y a un trait pour
  marquer l'absence d'une valeur numérique. Pour pouvoir représenter
  ce trait, Pandas a traité la colonne entière comme des données
  textuelles.

  Comme les données manquantes ont été supprimées, nous pouvons
  maintenant convertir la colonne 'inc' en entiers...

  ... et tenter de nouveau un plot.

  Faisons un zoom sur les dernière annnées pour mieux voir...

  Là on voit bien les pics en hiver et les creux en été.

+++
## Les points clés à retenir

- Pré-traitement des données
  + Adapter aux conventions des logiciels   <!-- .element: class="fragment" -->
  + Faciliter l'analyse   <!-- .element: class="fragment" -->
- Vérifier autant que possible   <!-- .element: class="fragment" -->
  + Inspection visuelle   <!-- .element: class="fragment" -->
  + Code de validation   <!-- .element: class="fragment" -->

Note:
  Voici les points clés à retenir de cette séquence.

  Après l'importation des données, il faut les adapter aux conventions
  des logiciels, dans notre cas la bibliothèque Pandas.  Ce
  pré-traitement facilite les analyses suivantes.
  
  Il est aussi important de vérifier les données autant que possible
  pour éviter que des erreurs passent inaperçues. L'inspection visuelle
  est une technique simple et puissante, mais il faut aussi utiliser
  du code.

---
# C028AL-M3-S5A

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/clap.png" style="width:50%; margin-top: none; border: none; background: none; box-shadow: none;" />

+++
### 3. La main à la pâte&emsp14;: une analyse réplicable

 1. Une analyse réplicable, c'est quoi&emsp14;?
 2. Étude de cas&emsp14;: l'incidence de syndromes grippaux
 3. Importer les données <br />
    (A&emsp14;: Jupyter/Python, B&emsp14;: RStudio, C&emsp14;: Org mode)
 4. Vérification et inspection <br />
    (A&emsp14;: Jupyter/Python, B&emsp14;: RStudio, C&emsp14;: Org mode)
 5. **Questions et réponses**  <br />
    (**A&emsp14;: Jupyter/Python**, B&emsp14;: RStudio, C&emsp14;: Org mode)

Note:
  Maintenant que nous sommes rassurés sur la qualité de nos données, nous
  pouvons poser des questions et obtenir des réponses par des calculs simples.

+++

## Questions

 1. Quelles années ont connu les épidémies les plus fortes&emsp14;?
 2. <!-- .element class="fragment" --> Quelle est la fréquence d'épidémies faibles, moyennes, et fortes&emsp14;?

Note:
  Notre première tâche est d'identifier les années avec les épidémies les plus fortes.

  Par la suite, nous allons élargir la question à la fréquence des épidémies en
  fonction de leur ampleur.
+++
# Démo

Note:
  Nous souhaitons analyser la variation des épidémies d'une année à
  l'autre, plutôt que la variation hebdomadaire détaillée. Nous devons
  donc sommer les incidences hebdomadaires année par année. Ceci n'est
  malheureusement pas si facile, parce qu'une année ne contient pas un
  nombre entier de semaines.

  Voici notre plan. Nous trouvons d'abord pour chaque année la semaine
  qui contient le 1er août, puis nous utilisons ces semaines comme
  débuts d'années dans notre sommation des incidences. En choisissant
  les limites des sommations dans les creux de l'été, nous minimisons
  le biais introduit dans notre analyse.

  Le 1er août en langage Pandas c'écrit comme ça, ici pour l'année 2000.

  La semaine qui contient le 1er août est la période qui inclut cette date
  et qui a la longueur d'une semaine. Une semaine Pandas commence toujours
  avec un dimanche.

  Nous remplaçons l'année 2000 par une variable, et faisons une boucle sur une
  intervalle qui commence avec la première année de notre jeu de données et
  se termine à la dernière.

  Chaque pair de semaines adjacentes dans notre liste décrit approximativement
  une année. Pour calculer les incidences annuelles, nous faisons une boucle sur
  ces pairs. A l'intérieur de cette boucle, nous sélectionnons les données
  correspondantes à cette intervalle qui commence avec la première semaine
  et se termine juste avant la deuxième, qui marque déjà le début de l'année
  suivante.

  Une petite remarque pour ceux qui connaissent bien le langage
  Python. On s'attendrait à écrire week1:week2, sans le -1. Mais
  Pandas ne suit pas cette convention autrement universelle dans le
  monde Python.
  
  Ici nous avons encore une occasion d'introduire une vérification.
  Dans notre sélection, il doit y avoir à peu près 52 semaines. Nous
  rajoutons une commande qui crée une erreur si la déviation de 52
  semaines dépasse une semaine.

  Maintenant nous pouvons calculer l'incidence annuelle simplement
  comme la somme sur notre sélection de données. Mais nous devons aussi
  noter l'année à laquelle cette somme correspond. Nous choisissons comme
  étiquette l'année de la semaine terminale, car le pic de l'épidémie
  se trouve autour du mois de février.

  Il faut encore stocker toutes les sommes et étiquettes calculées.
  Nous utilisons pour cela deux listes. A la fin, nous construisons
  une série de temps Pandas à partir de ces deux listes.
  
  Nous pouvons maintenant afficher un plot des incidences annuelles, en
  utilisant une étoile pour chaque année.
  
  Pour trouver les valeurs les plus élevées, qui correspondent aux
  années des plus fortes épidémies, le plus simple est d'afficher une
  liste triée des incidences annuelles. Nous trouvons les valeurs les
  plus élevées à la fin. Ce sont les années 1986, 1990, et 1989.
  
  Pour une vue plus globale de la distribution des épidémies, nous
  regardons un histogramme des incidences annuelles. L'argument `xrot=20`
  est juste pour tourner légèrement les étiquettes, pour les rendre plus
  lisibles.
  
  Nous voyons qu'en effet les trois valeurs les plus fortes correspondent
  à des épidémies exceptionnellement fortes. La plupart des épidémies sont
  bien plus faibles, avec seulement la moitié des incidences annuelles
  par rapport aux plus fortes.

  Jusqu'ici notre document computationnel ne contient que le code et les
  résultats des calculs. Comme nous l'avons déjà évoqué, il est tout aussi
  important d'y rajouter des explications, par exemple concernant la
  gestion des donnée manquantes ou le choix du 1er août comme date référence
  du début d'une année.

  Ceci fait partie d'une révision générale du notebook pour en faire un
  document utile et compréhensible pour un lecteur. Voici le document
  final, que nous mettons aussi à votre disposition pour une exploration
  personnelle. Nous avons rajouté beaucoup d'explications, mais aussi supprimé
  les quelques tentatives échoués qui n'apportent rien au lecteur, car notre
  but n'est pas de faire un cours du langage Python.
+++
## Les points clés à retenir

<p>Une analyse réplicable doit contenir **toutes les étapes** <br/>
   de traitement des données sous une forme **exécutable**.
</p>

<p class="fragment">Il est important d'**expliquer** tous les choix <br/>
                    qui peuvent influencer les résultats.
</p>

<p class="fragment">Ceci nécessite d'exposer beaucoup de **détails techniques**, <br />
                    parce que c'est à ce niveau qu'on fait **le plus d'erreurs**&emsp14;!
</p>


Note:
  Voici les points clés à retenir de ce module.
  
  Nous avons montré comment toutes les étapes d'une simple analyse de donnée
  peuvent être réalisées de façon réplicable.
  
  Une analyse réplicable doit contenir toutes les étapes du traitement
  des données sous une forme exécutable.

  Il faut également expliquer tous les choix qui ont un impact
  sur les résultats.

  Ceci vaut pour tous les détails techniques d'un calcul, comme dans notre
  exemple les manipulations des dates. Même si ces détails ne semblent pas
  essentiels pour comprendre l'analyse, ils sont des occasions
  pour faire des erreurs. Nous devons les exposer aux regards critiques
  pour assurer la transparence de notre travail.


---
# C028AL-M3-S3B

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/clap.png" style="width:50%; margin-top: none; border: none; background: none; box-shadow: none;" />

+++
### 3. La main à la pâte&emsp14;: une analyse réplicable

 1. Une analyse réplicable, c'est quoi&emsp14;?
 2. Étude de cas&emsp14;: l'incidence de syndromes grippaux
 3. **Importer les données**  <br />
    (A&emsp14;: Jupyter/Python, **B&emsp14;: RStudio**, C&emsp14;: Org mode)
 4. Vérification et inspection <br />
    (A&emsp14;: Jupyter/Python, B&emsp14;: RStudio, C&emsp14;: Org mode)
 5. Questions et réponses   <br />
    (A&emsp14;: Jupyter/Python, B&emsp14;: RStudio, C&emsp14;: Org mode)

Note:
  Dans cette séquence, nous montrons comment créer un document
  computationnel et y importer nos données sur l'incidence du syndrome
  grippal.


+++
## Choix techniques

 - Environnement RStudio
 - Langage R   <!-- .element: class="fragment" -->
 - Bibliothèque&emsp14;: parsedate <!-- .element: class="fragment" -->

Note:
  Nous devons d'abord faire quelques choix techniques:
  - Notre document computationnel est rédigé avec RStudio
  - Le langage de programmation est R
  - Nous utilisons la bibliothèque parsedate pour la gestion
    des numéros de semaines ISO

+++
# Démo

Note:

  Dans RStudio, nous créons un nouveau fichier du type "R Markdown".
  Nous lui donnons le titre "Analyse de l'incidence du syndrôme grippal".
  Le nouveau fichier n'est pas vide: les métadonnées sont préremplies,
  et il y a deux courtes sections qui illustrent comment écrire
  en R Markdown. Nous les effaçons pour mettre notre propre contenu
  à la place.
  
  R peut lire directement le format CSV. Pour cela nous avons
  besoin de l'URL de téléchargement de nos données. Nous l'obtenons de
  l'historique du navigateur que nous avons utilisé pour télécharger
  le fichier précédemment.
  
  Il faut encore rajouter le paramètre `skip=1`, qui indique à
  R d'ignorer la première ligne du fichier, qui contient un
  commentaire.
  
  Nous pouvons maintenant jeter un coup d'oeil sur nos données.
  D'abord les premières lignes...
  ...puis les dernières.
  Pour l'instant, il semble que tout s'est bien passé.

  Nous allons maintenant regarder s'il y a des données manquantes.
  Nous demandons d'abord à R de trouver les lignes du tableau dans
  lesquelles au moins une valeur est 'na', not available, donc
  absente. Puis nous demandons d'afficher ces lignes. Et voilà, nous
  retrouvons la semaine 19 de l'année 1989.
  
  Nous vérifions enfin les deux colonnes qui nous intéressent, week
  et inc. Leurs classes sont... "integer" pour "week", ce qui est bien...
  mais "factor" pour "inc", ce qui indique un problème.
  
  La cause est le point manquant, qui contient un tiré comme valeur de
  "inc", plutôt que rien du tout. La façon la plus simple pour gérer ceci
  en R est de relire nos données en précisant que "-" est à traiter comme
  valeur na.
  
  Maintenant les deux colonnes que nous allons traiter sont de classe "integer".

+++
## Les points clés à retenir

 - Lecture des données directement de la source
 - <!-- .element class="fragment" -->Attention aux données manquantes

Note:
  Nous rappelons les points clés à retenir de cette séquence.

  Premièrement, il faut lire les données directement de la source.

  Deuxièmement, il faut faire attention aux données manquantes et notamment
  sur la façon dont elles sont traitées lors de la lecture.

---
# C028AL-M3-S4B

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/clap.png" style="width:50%; margin-top: none; border: none; background: none; box-shadow: none;" />

+++
### 3. La main à la pâte&emsp14;: une analyse réplicable

 1. Une analyse réplicable, c'est quoi&emsp14;?
 2. Étude de cas&emsp14;: l'incidence de syndromes grippaux
 3. Importer les données <br />
    (A&emsp14;: Jupyter/Python, B&emsp14;: RStudio, C&emsp14;: Org mode)
 4. **Vérification et inspection** <br />
    (A&emsp14;: Jupyter/Python, **B&emsp14;: RStudio**, C&emsp14;: Org mode)
 5. Questions et réponses   <br />
    (A&emsp14;: Jupyter/Python, B&emsp14;: RStudio, C&emsp14;: Org mode)

Note:
  Dans cette séquence, nous préparons les données pour notre analyse.
  Ceci implique notamment de vérifier et inspecter les données,
  pour détecter des anomalies et des erreurs dans l'importation.

+++
# Démo

Note:

  Pour pouvoir travailler avec nos données, nous devons modifier la
  représentation des dates, parce que R ne comprend pas le format
  assez inhabituel utilisé par le Réseau Sentinelles. Nous montrons
  cette conversion d'abord pour une seule valeur.

  D'abord nous convertissons la valeur initiale en chaîne de
  caractères.  Nous la réarrangeons pour arriver au format précis
  prévu par la norme ISO 8601, qui prévoit que le numéro de semaine
  soit préfixé par un W. Maintenant la bibliothèque parsedate peut
  l'interpréter et convertir en date et heure, en utilisant la
  convention POSIX, donc en nombre de secondes écoulées depuis le
  1er janvier 1970. Pour faire cette conversion, il faut rajouter
  de l'information sur le jour de la semaine et l'heure. Par défaut,
  c'est le lundi, premier jour de la semaine ISO, et minuit.
  
  Pour appliquer cette conversion à toutes les lignes de notre tableau,
  nous en faisons une fonction R.

  Maintenant nous pouvons tenter de rajouter une nouvelle colonne "date"
  à notre jeu de données. Malheureusement, ça ne marche pas...
  Le problème de fond est que l'utilisation de sapply mène à une
  conversion automatique de nos dates en entiers. Dans cette conversion,
  l'information que ces entiers représentent le nombre de secondes depuis
  le 1er janvier 1970 est perdue. Pour contourner cette perte d'information,
  nous convertissons nos dates en texte.
  
  Maintenant nous avons bien des valeurs de classe "Date".
  
  Pour notre analyse, il est plus pratique de travailler avec une
  suite d'observations dans l'ordre chronologique, donc nous
  appliquons un tri.
  
  Il est toujours conseillé de vérifier autant que possible la
  cohérence des données à chaque étape du traitement. Ici nous
  pouvons vérifier que des points voisins dans notre jeu de données sont
  séparés d'exactement 7 jours.

  Regardons enfin un plot de nos données !
  
  Faisons un zoom sur les dernière annnées pour mieux voir...

  Là on voit bien les pics en hiver et les creux en été.

+++
## Les points clés à retenir

- Pré-traitement des données
  + Adapter aux conventions des logiciels   <!-- .element: class="fragment" -->
  + Faciliter l'analyse   <!-- .element: class="fragment" -->
- Vérifier autant que possible   <!-- .element: class="fragment" -->
  + Inspection visuelle   <!-- .element: class="fragment" -->
  + Code de validation   <!-- .element: class="fragment" -->

Note:
  Voici les points clés à retenir de cette séquence.

  Après l'importation des données, il faut les adapter aux conventions
  des logiciels.  Ce pré-traitement facilite les analyses suivantes.
  Ici nous avons converti les dates et trié les données dans l'ordre
  chronologique.
  
  Il est aussi important de vérifier les données autant que possible
  pour éviter que des erreurs passent inaperçues. L'inspection visuelle
  est une technique simple et puissante, mais il faut aussi utiliser
  du code.

---
# C028AL-M3-S5B

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/clap.png" style="width:50%; margin-top: none; border: none; background: none; box-shadow: none;" />

+++
### 3. La main à la pâte&emsp14;: une analyse réplicable

 1. Une analyse réplicable, c'est quoi&emsp14;?
 2. Étude de cas&emsp14;: l'incidence de syndromes grippaux
 3. Importer les données <br />
    (A&emsp14;: Jupyter/Python, B&emsp14;: RStudio, C&emsp14;: Org mode)
 4. Vérification et inspection <br />
    (A&emsp14;: Jupyter/Python, B&emsp14;: RStudio, C&emsp14;: Org mode)
 5. **Questions et réponses**  <br />
    (A&emsp14;: Jupyter/Python, **B&emsp14;: RStudio**, C&emsp14;: Org mode)

Note:
  Maintenant que nous sommes rassurés sur la qualité de nos données, nous
  pouvons poser des questions et obtenir des réponses par des calculs simples.

+++

## Questions

 1. Dans quelles années y avait-il les épidémies les plus fortes&emsp14;?
 2. <!-- .element class="fragment" --> Quelle est la fréquence d'épidémies faibles, moyennes, et fortes&emsp14;?

Note:
  Notre première tâche est d'identifier les années avec les épidémies les plus fortes.

  Par la suite, nous allons élargir la question à la fréquence des épidémies en
  fonction de leur ampleur.
+++

## Démo

&nbsp;

Note:
  Nous souhaitons analyser la variation des épidémies d'une année à
  l'autre, plutôt que la variation hebdomadaire détaillée. Nous devons
  donc sommer les incidences hebdomadaires année par année. Ceci n'est
  malheureusement pas si facile, parce qu'une année ne contient pas un
  nombre entier de semaines. En plus, l'année civile n'est pas un bon
  choix pour la sommation, car les pics épidemiques commencent à la
  fin d'une année civile et se poursuivent l'année suivante.

  Voici notre plan. Nous allons commencer et terminer nos années aux
  1er août. Cette date se trouve dans le creux de l'incidence, donc
  une petite imprécision de date n'a pas de conséquence importante.
  Plus précisément, nous définissons le pic de l'année N comme la somme
  des incidences entre le 1er août de l'année N-1 et le 1er août de l'année N.
  Traduisons cela en une fonction R.
  
  D'abord nous construisons la date du 1er août de l'année N-1...
  ... puis la date du 1er août de l'année N.
  Les semaines à choisir pour la sommation sont celle dont la date du lundi
  tombe entre ces deux limites.
  Il ne reste qu'à sommer l'incidence de ces semaines, en demandant à R 
  d'ignorer les valeur 'na', donc les points manquants. Nous savons que
  ceci concerne un seul point en 1989, donc le pic de cette année sera
  sous-estimé.

  Nous devons aussi faire attention aux premières et dernières années
  de notre jeux de données. Les données commencent en janvier 1985, ce
  qui ne permet pas de quantifier complètement le pic attribué à cette
  année. Nous le supprimons donc de notre analyse. Par contre, les
  données se terminent en été 2017, peu avant le 1er août, ce qui nous
  permet d'inclure cette année dans l'analyse.

  Nous créons un nouveau jeu de données pour l'incidence annuelle, en
  applicant la fonction `pic_annuel` à chaque année.
  
  Et voici un plot des incidences annuelles. Nous voyons que trois
  années ont eu des épidémies bien plus fortes que les autres.
  
  Une liste triée par ordre décroissant d'incidence annuelle permet de
  repérer facilement les valeurs les plus élevées.

  Enfin, un histogramme montre bien que les épidémies fortes, qui
  touchent environ 10% de la population française, sont assez rares:
  il y en eu trois au cours des 35 dernières années.
  
+++
## Les points clés à retenir

<p>Une analyse réplicable doit contenir **toutes les étapes** <br/>
   de traitement des données sous une forme **exécutable**.
</p>

<p class="fragment">Il est important d'**expliquer** tous les choix <br/>
                    qui peuvent influencer les résultats.
</p>

<p class="fragment">Ceci nécessite d'exposer beaucoup de **détails techniques**, <br />
                    parce que c'est à ce niveau qu'on fait **le plus d'erreurs**&emsp14;!
</p>


Note:
  Voici les points clés à retenir de ce module.
  
  Nous avons montré comment toutes les étapes d'une simple analyse de donnée
  peuvent être réalisées de façon réplicable.
  
  Une analyse réplicable doit contenir toutes les étapes du traitement
  des données sous une forme exécutable.

  Il faut également expliquer tous les choix qui ont un impact
  sur les résultats.

  Ceci vaut pour tous les détails techniques d'un calcul, comme dans notre
  exemple les manipulations des dates. Même si ces détails ne semblent pas
  essentiels pour comprendre l'analyse, ils sont des occasions
  pour faire des erreurs. Nous devons les exposer aux regards critiques
  pour assurer la transparence de notre travail.


---
# C028AL-M3-S3C

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/clap.png" style="width:50%; margin-top: none; border: none; background: none; box-shadow: none;" />

+++
### 3. La main à la pâte&emsp14;: une analyse réplicable

 1. Une analyse réplicable, c'est quoi&emsp14;?
 2. Étude de cas&emsp14;: l'incidence de syndromes grippaux
 3. **Importer les données**  <br />
    (A&emsp14;: Jupyter/Python, B&emsp14;: RStudio, **C&emsp14;: Org mode**)
 4. Vérification et inspection <br />
    (A&emsp14;: Jupyter/Python, B&emsp14;: RStudio, C&emsp14;: Org mode)
 5. Questions et réponses   <br />
    (A&emsp14;: Jupyter/Python, B&emsp14;: RStudio, C&emsp14;: Org mode)

Note:
  Dans cette séquence, nous montrons comment créer un document
  computationnel et y importer nos données sur l'incidence du syndrome
  grippal.


+++
## Choix techniques

 - Environnement Emacs + Org mode
 - Langages&emsp14;: <br />
   &nbsp;&nbsp;&nbsp;&nbsp; Python 3 pour préparer les données <br />
   &nbsp;&nbsp;&nbsp;&nbsp; R pour l'analyse <!-- .element: class="fragment" -->

Note:
  Nous devons d'abord faire quelques choix techniques:
  - Notre document computationnel est un fichier org-mode rédigé avec Emacs
  - Les langages de programmation sont Python dans sa version 3 et R

  Nous utilisons deux langages pour montrer comment org-mode rend ceci possible.
  C'est aussi l'occasion de choisir le meilleur langage pour chaque tâche.
  Nous utilisons Python pour la première partie, où nous profitons du
  fait que Python, depuis la version 3.6, sait traiter les numéros de semaine
  en format ISO. Par la suite, nous passons à R qui rend l'analyse et
  les plots plus faciles.
  
+++
# Démo

Note:

  Nous commençons en créant un nouveau fichier org-mode dans Emacs.
  
  Pour télécharger les données, nous avons besoin de l'URL
  correspondant. Nous l'obtenons de l'historique du navigateur que
  nous avons utilisé pour télécharger le fichier précédemment.
  
  Maintenant nous créons notre premier morceau de code Python.
  Voici à quoi ça ressemble. Nous rajoutons une variable org-mode
  pour passer l'URL des données.
  
  Le code lit d'abord les données comme une longue chaîne d'octets.
  Il faut décoder les octets pour obtenir une chaîne de caractères,
  dont nous enlevons des blancs au début et à la fin avant de la
  découper en lignes.
  
  La première ligne contient un commentaire, nous ne l'utilisons pas.
  
  Les autres lignes doivent être découpées en colonnes.
  
  Ce code est vite exécuté... et le résultat est None, parce que nous n'avons
  pas défini un résultat explicitement. Dans ce cas, il vaut mieux ne pas
  afficher le résultat, ce qui nous faisons en précisons :results silent
  dans l'en-tête.

  Regardons ce que nous avons obtenu, avec un deuxième morceau de code
  très court. Org-mode interprète la liste de listes que nous avons
  créée en Python comme un tableau et l'affiche proprement.

  Il y a deux colonnes qui nous intéressent: la première (~"week"~) et
  la troisième (~"inc"~). Nous vérifions leurs noms dans l'en-tête,
  que nous effaçons par la suite. Enfin, nous créons un tableau avec
  les deux colonnes pour le traitement suivant.

  Regardons les premières et les dernières lignes. Nous insérons
  ~None~ pour indiquer à org-mode la séparation entre les trois
  sections du tableau: en-tête, début des données, fin des données.

  Il est toujours prudent de vérifier si les données semblent
  crédibles. Nous savons que les semaines sont données par six
  chiffres (quatre pour l'année et deux pour la semaine), et que les
  incidences sont des nombres entiers positifs.
  
  La vérification a mis en évidence un point manquant dans le jeux de
  données. Nous l'éliminons, ce qui n'a pas d'impact fort sur notre
  analyse qui est assez simple.
  
+++
## Les points clés à retenir

 - Lecture des données directement de la source
 - <!-- .element class="fragment" -->Gestion des données manquantes

Note:
  Nous rappelons les points clés à retenir de cette séquence.

  Premièrement, il faut lire les données directement de la source.

  Deuxièmement, il faut faire attention aux données manquantes.

---
# C028AL-M3-S4C

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/clap.png" style="width:50%; margin-top: none; border: none; background: none; box-shadow: none;" />

+++
### 3. La main à la pâte&emsp14;: une analyse réplicable

 1. Une analyse réplicable, c'est quoi&emsp14;?
 2. Étude de cas&emsp14;: l'incidence de syndromes grippaux
 3. Importer les données <br />
    (A&emsp14;: Jupyter/Python, B&emsp14;: RStudio, C&emsp14;: Org mode)
 4. **Vérification et inspection** <br />
    (A&emsp14;: Jupyter/Python, B&emsp14;: RStudio, **C&emsp14;: Org mode**)
 5. Questions et réponses   <br />
    (A&emsp14;: Jupyter/Python, B&emsp14;: RStudio, C&emsp14;: Org mode)

Note:
  Dans cette séquence, nous préparons les données pour notre analyse.
  Ceci implique notamment de vérifier et inspecter les données,
  pour détecter des anomalies et des erreurs dans l'importation.

+++
# Démo

Note:

  Pour faciliter les traitements suivants, que nous allons faire en
  langage R, nous remplaçons les numéros de semaine ISO par les dates
  qui correspondent aux lundis. A cette occasion, nous trions aussi
  les données dans l'ordre chronologique, et nous transformons les
  incidences en nombres entiers.

  Regardons de nouveau  premières et les dernières lignes. Org-mode
  ne comprend que les nombres et les chaînes de caractères. Nous devons
  donc convertir les dates en texte.

  Il est toujours conseillé de vérifier autant que possible la
  cohérence des données à chaque étape du traitement. Ici nous
  pouvons vérifier que des points voisins dans notre jeu de données sont
  séparés d'exactement 7 jours.

  Nous passons au langage R pour inspecter nos données. Mais d'abord
  nous devons exporter ces données en Python sous une forme
  qu'org-mode peut comprendre et traduire en R. Ceci nécessite aussi
  qu'on donne un nom à ce morceau de code. Comme le jeu de données est
  grand, nous ne l'affichons pas.

  Notre premier morceau de code en R reçoit une variable d'entrée par org-mode.
  Les données arrivent sous forme d'un tableau à deux colonnes. Les dates
  sont stockées comme textes, nous les devons alors convertir en vraies dates.
  Les incidences sont déjà des entiers.
  
  Regardons enfin un plot de nos données !
  
  Faisons un zoom sur les dernière annnées pour mieux voir...

  Là on voit bien les pics en hiver et les creux en été.

+++
## Les points clés à retenir

- Pré-traitement des données
  + Adapter aux conventions des logiciels   <!-- .element: class="fragment" -->
  + Faciliter l'analyse   <!-- .element: class="fragment" -->
- Vérifier autant que possible   <!-- .element: class="fragment" -->
  + Inspection visuelle   <!-- .element: class="fragment" -->
  + Code de validation   <!-- .element: class="fragment" -->

Note:
  Voici les points clés à retenir de cette séquence.

  Après l'importation des données, il faut les adapter aux conventions
  des logiciels. Ce pré-traitement facilite les analyses suivantes.
  
  Il est aussi important de vérifier les données autant que possible
  pour éviter que des erreurs passent inaperçues. L'inspection visuelle
  est une technique simple et puissante, mais il faut aussi utiliser
  du code.

---
# C028AL-M3-S5C

<img src="https://rawgit.com/alegrand/RR_MOOC/master/assets/img/clap.png" style="width:50%; margin-top: none; border: none; background: none; box-shadow: none;" />

+++
### 3. La main à la pâte&emsp14;: une analyse réplicable

 1. Une analyse réplicable, c'est quoi&emsp14;?
 2. Étude de cas&emsp14;: l'incidence de syndromes grippaux
 3. Importer les données <br />
    (A&emsp14;: Jupyter/Python, B&emsp14;: RStudio, C&emsp14;: Org mode)
 4. Vérification et inspection <br />
    (A&emsp14;: Jupyter/Python, B&emsp14;: RStudio, C&emsp14;: Org mode)
 5. **Questions et réponses**  <br />
    (A&emsp14;: Jupyter/Python, B&emsp14;: RStudio, **C&emsp14;: Org mode**)

Note:
  Maintenant que nous sommes rassurés sur la qualité de nos données, nous
  pouvons poser des questions et obtenir des réponses par des calculs simples.

+++

## Questions

 1. Dans quelles années y avait-il les épidémies les plus fortes&emsp14;?
 2. <!-- .element class="fragment" --> Quelle est la fréquence d'épidémies faibles, moyennes, et fortes&emsp14;?

Note:
  Notre première tâche est d'identifier les années avec les épidémies les plus fortes.

  Par la suite, nous allons élargir la question à la fréquence des épidémies en
  fonction de leur ampleur.
+++
# Démo

Note:

  Nous souhaitons analyser la variation des épidémies d'une année à
  l'autre, plutôt que la variation hebdomadaire détaillée. Nous devons
  donc sommer les incidences hebdomadaires année par année. Ceci n'est
  malheureusement pas si facile, parce qu'une année ne contient pas un
  nombre entier de semaines. En plus, l'année civile n'est pas un bon
  choix pour la sommation, car les pics épidemiques commencent à la
  fin d'une année civile et se poursuivent l'année suivante.

  Voici notre plan. Nous allons commencer et terminer nos années aux
  1er août. Cette date se trouve dans le creux de l'incidence, donc
  une petite imprécision de date n'a pas de conséquence importante.
  Plus précisément, nous définissons le pic de l'année N comme la somme
  des incidences entre le 1er août de l'année N-1 et le 1er août de l'année N.
  Traduisons cela en une fonction R.
  
  D'abord nous construisons la date du 1er août de l'année N-1...
  ... puis la date du 1er août de l'année N.
  Les semaines à choisir pour la sommation sont celle dont la date du lundi
  tombe entre ces deux limites.
  Il ne reste qu'à sommer l'incidence de ces semaines, en demandant à R 
  d'ignorer les valeur 'na', donc les points manquants. Nous savons que
  ceci concerne un seul point en 1989, donc le pic de cette année sera
  sous-estimé.

  Nous devons aussi faire attention aux premières et dernières années
  de notre jeux de données. Les données commencent en janvier 1985, ce
  qui ne permet pas de quantifier complètement le pic attribué à cette
  année. Nous le supprimons donc de notre analyse. Par contre, les
  données se terminent en été 2017, peu avant le 1er août, ce qui nous
  permet d'inclure cette année dans l'analyse.

  Nous créons un nouveau jeu de données pour l'incidence annuelle, en
  applicant la fonction `pic_annuel` à chaque année.
  
  Et voici un plot des incidences annuelles. Nous voyons que trois
  années ont eu des épidémies bien plus fortes que les autres.
  
  Une liste triée par ordre décroissant d'incidence annuelle permet de
  repérer facilement les valeurs les plus élevées.

  Enfin, un histogramme montre bien que les épidémies fortes, qui
  touchent environ 10% de la population française, sont assez rares:
  il y en eu trois au cours des 35 dernières années.

+++
## Les points clés à retenir

<p>Une analyse réplicable doit contenir **toutes les étapes** <br/>
   de traitement des données sous une forme **exécutable**.
</p>

<p class="fragment">Il est important d'**expliquer** tous les choix <br/>
                    qui peuvent influencer les résultats.
</p>

<p class="fragment">Ceci nécessite d'exposer beaucoup de **détails techniques**, <br />
                    parce que c'est à ce niveau qu'on fait **le plus d'erreurs**&emsp14;!
</p>


Note:
  Voici les points clés à retenir de ce module.
  
  Nous avons montré comment toutes les étapes d'une simple analyse de donnée
  peuvent être réalisées de façon réplicable.
  
  Une analyse réplicable doit contenir toutes les étapes du traitement
  des données sous une forme exécutable.

  Il faut également expliquer tous les choix qui ont un impact
  sur les résultats.

  Ceci vaut pour tous les détails techniques d'un calcul, comme dans notre
  exemple les manipulations des dates. Même si ces détails ne semblent pas
  essentiels pour comprendre l'analyse, ils sont des occasions
  pour faire des erreurs. Nous devons les exposer aux regards critiques
  pour assurer la transparence de notre travail.
