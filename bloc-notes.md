# Bloc-notes

Ce document regroupe des ressources externes potentiellement utile pour nous.

## Canaux de diffusion pour annoncer le MOOC

- CNRS hebdo (un par délégation)

## Exemples de non-reproductibilité

- [Zones de danger en R] (http://blog.appsilondatascience.com/rstats/2017/03/28/reproducible-research-when-your-results-cant-be-reproduced.html)

- [Notebooks Jupyter/Python non-reproductibles](https://markwoodbridge.com/2017/03/05/jupyter-reproducible-science.html), avec discussion [sur GitHub](https://github.com/sparcopen/open-research-doathon/issues/25)

- [Discussion](https://github.com/ReScience/ReScience/issues/43) sur la difficulté de reproduire les publications supposées reproductibles dans ReScience

- [Non-reproductibilité](https://f1000research.com/articles/6-124/v1#referee-response-20292) d'un exemple pour une analyse reproductible

## Exemples de publications reproductibles "grand public"

- ["Wages rise on California farms. Americans still don’t want the job"](http://www.latimes.com/projects/la-fi-farms-immigration/), un article du Los Angeles Times basé sur une [analyse de données publiée sur GitHub](https://github.com/datadesk/california-crop-production-wages-analysis)

## Conseils pour rendre un travail reproductible

- [A very simple, re-executable neuroimaging publication](https://f1000research.com/articles/6-124/v1) (un article qui décrit comment publier un calcul reproductible, en échouant à la première tentative)

## Cahiers de laboratoire

- [Informations du CNRS](http://www.cnrs.fr/infoslabos/cahier-laboratoire/) sur la gestion d'un cahier de laboratoire, y compris un [modèle en PDF](http://www.cnrs.fr/infoslabos/cahier-laboratoire/docs/cahierlabo.pdf)

### Cahiers de laboratoire de célébrités

- [Marie Curie](https://explore.univ-psl.fr/fr/digitized-collection/cahiers-de-bord-et-rapports-d%E2%80%99activit%C3%A9s-du-laboratoire-curie)
- [Michael Faraday](http://www.faradaysdiary.com/)
- [Linus Pauling](http://scarc.library.oregonstate.edu/coll/pauling/rnb/index.html)
